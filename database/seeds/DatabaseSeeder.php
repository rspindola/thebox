<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => 'Akira',
            'email' => 'e.akira@live.com',
            'role' => '1',
            'status' => '1',
            'password' => bcrypt('akira')
        ]);
        factory(App\User::class, 9)->create();
    }
}
