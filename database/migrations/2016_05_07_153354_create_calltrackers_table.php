<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalltrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calltrackers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origem')->nullable();
            $table->string('telefone')->nullable();
            $table->string('duracao')->nullable();
            $table->string('audio')->nullable();
            $table->string('rota')->nullable();
            $table->integer('pro_id')->unsigned()->index();
            $table->foreign('pro_id')->references('id')->on('projetos')->onDelete('cascade');
            $table->timestamp('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calltrackers');
    }
}
