<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetoUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projeto_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usu_id')->unsigned()->index();
            $table->foreign('usu_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('pro_id')->unsigned()->index();
            $table->foreign('pro_id')->references('id')->on('projetos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projeto_usuarios');
    }
}
