<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsValoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads_valores', function (Blueprint $table) {
            $table->increments('id');
            $table->date('inicio');
            $table->date('fim');
            $table->integer('total');
            $table->decimal('investido', 10, 2);
            $table->decimal('final', 10, 2);
            $table->decimal('meta', 10, 2);
            $table->decimal('variacao', 10, 2);
            $table->integer('lea_id')->unsigned()->index();
            $table->foreign('lea_id')->references('id')->on('leads')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leads_datas');
    }
}
