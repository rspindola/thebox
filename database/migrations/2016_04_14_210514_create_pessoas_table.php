<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('mensagem');
            $table->string('coords');
            $table->string('origem');
            $table->string('peca');
            $table->string('campaign');
            $table->string('content');
            $table->string('term');
            $table->string('sistema');
            $table->integer('pro_id')->unsigned()->index();
            $table->foreign('pro_id')->references('id')->on('projetos')->onDelete('cascade');
            $table->integer('res_id')->unsigned()->index()->nullable();
            $table->foreign('res_id')->references('id')->on('responsaveis')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pessoas');
    }
}
