<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImobiliariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imobiliarias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imobiliaria');
            $table->integer('res_id')->unsigned()->index()->nullable();
            $table->foreign('res_id')->references('id')->on('responsaveis')->onDelete('cascade');
            $table->integer('pro_id')->unsigned()->index();
            $table->foreign('pro_id')->references('id')->on('projetos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imobiliarias');
    }
}
