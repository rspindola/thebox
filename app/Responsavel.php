<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsavel extends Model
{
	protected $fillable = array('responsavel', 'email', 'imo_id');

    protected $table = 'responsaveis';

	public function imobiliaria()
	{
		return $this->belongsTo('App\Imobiliaria', 'imo_id');
	}

	public function pessoas()
	{
		return $this->hasMany('App\Pessoas', 'res_id');
	}

}
