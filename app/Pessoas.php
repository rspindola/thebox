<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoas extends Model
{
	protected $fillable = array('nome', 'email', 'telefone', 'mensagem', 'coords', 'origem', 'peca', 'campaing', 'content', 'term', 'sistema', 'data', 'pro_id', 'res_id');

	public function projeto()
	{
		return $this->belongsTo('App\Projeto');
	}

	public function responsavel()
	{
		return $this->belongsTo('App\Responsavel', 'res_id');
	}
}
