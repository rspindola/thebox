<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadsValores extends Model
{
	protected $fillable = array('inicio', 'fim', 'total', 'investido', 'final', 'meta', 'variacao', 'lea_id');


    /**
     * Formatar numeros 
     */


    public function setInvestidoAttribute($value)
    {
    	$value = str_replace(".","",$value);;
	    $value = str_replace(",",".",$value);
        $this->attributes['investido'] = $value;
    }

    public function setMetaAttribute($value)
    {
    	$value = str_replace(".","",$value);;
	    $value = str_replace(",",".",$value);
        $this->attributes['meta'] = $value;
    }

    public function getInvestidoAttribute($value)
    {
        return number_format($value,2,",",".");
    }
    public function getMetaAttribute($value)
    {
        return number_format($value,2,",",".");
    }


    public function getInicioAttribute($value)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function getFimAttribute($value)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function setInicioAttribute($value)
    {
        $value = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        $this->attributes['inicio'] = $value;
    }

    public function setFimAttribute($value)
    {
        $value = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        $this->attributes['fim'] = $value;
    }






    /**
    * Relacionamentos
    */
    public function leads()
    {
        return $this->belongsTo('App\Leads');
    }


}
