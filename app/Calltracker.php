<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calltracker extends Model
{
	protected $fillable = array('origem', 'telefone', 'responsavel', 'pro_id', 'rota', 'data', 'duracao', 'audio');

	public function projeto()
	{
		return $this->belongsTo('App\Projeto');
	}

    public function getDataAttribute($date)
    {	
    	return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i:s');
    }


}
