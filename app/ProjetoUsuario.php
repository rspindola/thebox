<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjetoUsuario extends Model
{
	protected $fillable = array('usu_id', 'pro_id');
}
