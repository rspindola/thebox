<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjetoMeta extends Model
{
	protected $fillable = array('meta', 'pro_id');

	public function projeto()
	{
		return $this->belongsTo('App\Projeto');
	}
}
