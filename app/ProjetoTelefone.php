<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjetoTelefone extends Model
{
	protected $fillable = array('telefone', 'pro_id');

	public function projeto()
	{
		return $this->belongsTo('App\Projeto');
	}

}
