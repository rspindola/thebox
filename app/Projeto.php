<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
	protected $fillable = array('projeto', 'status', 'analytics', 'codigo', 'created_at');


    protected $table = 'projetos';


    public function getCreatedAtAttribute($date)
    {	
    	return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i:s');
    }




    
	public function telefone()
	{
		return $this->hasMany('App\ProjetoTelefone', 'pro_id');
	}
	public function meta()
	{
		return $this->hasMany('App\ProjetoMeta', 'pro_id');
	}
	public function usuario()
	{
        return $this->belongsToMany('App\User','projeto_usuarios', 'pro_id', 'usu_id');
	}

	public function imobiliaria()
	{
		return $this->hasMany('App\Imobiliaria', 'pro_id');
	}
	public function pessoas()
	{
		return $this->hasMany('App\Pessoas', 'pro_id');
	}
	public function leads()
	{
		return $this->hasMany('App\Leads', 'pro_id');
	}
	public function calltracker()
	{
		return $this->hasMany('App\Calltracker', 'pro_id');
	}
}