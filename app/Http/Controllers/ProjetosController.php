<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;




use App\Http\Requests\ProjetosRequest;
use App\Projeto;
use App\ProjetoTelefone;
use App\ProjetoMeta;
use App\Imobiliaria;
use App\Responsavel;
use App\Pessoas;
use App\Leads;
use App\LeadsValores;
class ProjetosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */





    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */



    public function index()
    {
        $Projeto = Projeto::all();
        return view('projetos.index',compact('Projeto'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // Dados em branco para aparecer o primeiro registro
        $Dados = array('imobiliaria' => array(array('imobiliaria' => '', 'id' => '', 'responsaveis' => array(array('id' => '', 'responsavel' => '', 'email' => '')))));


        return view('projetos.create', compact('Dados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjetosRequest $request)
    {

        $Projeto    = Projeto::create($request->all());
        $pro_id     = $Projeto->id;

        $Leads  = Leads::create(['origem' => 'Calltracker','pro_id' => $pro_id]);
        LeadsValores::create(['inicio' => date('d/m/Y'), 'fim' => date('d/m/Y'), 'lea_id' => $Leads->id]);

        $this->Salvar($request, $pro_id);

        return redirect('projetos');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $Dados          = Projeto::with('meta', 'telefone', 'imobiliaria', 'imobiliaria.responsaveis', 'usuario')->get()->find($id)->toArray();
        return view('projetos.edit',compact('Dados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjetosRequest $request, $id)
    {

        $Imobiliaria=Projeto::find($id);
        $Imobiliaria->update($request->all());
        $this->Salvar($request, $id);

        return redirect('projetos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Projeto::find($id)->delete();
        return redirect('projetos');
    }





    public function tag()
    {
        $Dados = Projeto::select(['id', 'projeto'])->get();

        $Aux = array();
        foreach ($Dados as $Array) {
            $Aux[] = array('value' => $Array->id, 'text' => $Array->projeto);
        }
        return $Aux;
    }

    public function tagselecionadas($id)
    {

        $Dados = Projeto::find($id)->usuario()->get();        
        
        $Aux = array();
        foreach ($Dados as $Array) {
            $Aux[] = array('value' => $Array->id, 'text' => $Array->name);
        }
        return $Aux;
    }



    /** 
    * Funcao para salvar a imobiliaria e o responsavel
    * Primeiro faz um foreach com a imobiliaria caso tenha o imo_id (input) faz um upgrade se não cria
    *
    *
    */
    public function Salvar(Request $request, $pro_id){

        $input = $request->input();



        // Salvar o telefone
        ProjetoTelefone::where('pro_id', '=', $pro_id)->delete();
        $Telefones  = explode(',', $input['calltracker']);
        $Dados      = array();
        foreach ($Telefones as $key => $value) {
            if($value) // Caso nao tenha telefone nao salva
            $Dados[] = array('telefone' => $value, 'pro_id' => $pro_id);
        }

        ProjetoTelefone::insert($Dados);



        ProjetoMeta::where('pro_id', '=', $pro_id)->delete();
        $Metas  = explode(',', $input['meta']);
        $Dados      = array();
        foreach ($Metas as $key => $value) {
            if($value) // Caso nao tenha telefone nao salva
            $Dados[] = array('meta' => $value, 'pro_id' => $pro_id);
        }

        ProjetoMeta::insert($Dados);

        // Salva os Usuarios

        if($input['usuarios']) {
            $usuarios  = explode(',', $input['usuarios']);
            Projeto::find($pro_id)->usuario()->sync($usuarios);
        }






        // Salvar a Imobiliaria
        $ImobiliariasAtual  = Imobiliaria::where('pro_id', '=', $pro_id)->get()->toArray();
        
        // Cria um array com as imobiliarias atuais para depois destruir elas

        if($ImobiliariasAtual) {
            $Aux = array();
            foreach ($ImobiliariasAtual as $Array) {
                $Aux[$Array['id']] = $Array['id'];
            }
            $ImobiliariasAtual = $Aux;
        }

        
        foreach($input['imobiliaria'] as $key => $value) {

            $imo_id     = $input['imo_id'][$key];


            if($value) {// Se exite o valor adiciona
                // Caso nao tenha um id cria a imobiliaria
                if(!$imo_id)// Cria a Imobiliaria
                {
                    $Imobiliaria    = Imobiliaria::create([
                        'imobiliaria' => $value,
                        'pro_id' => $pro_id
                        ]);
                    $imo_id         =  $Imobiliaria->id;

                } 
                else
                {
                    unset($ImobiliariasAtual[$imo_id]);
                    $Imobiliaria=Imobiliaria::find($imo_id);
                    $Imobiliaria->update([
                        'imobiliaria' => $value
                        ]);

                }
            }




            // Cria um array com as imobiliarias atuais para depois destruir elas
            $ResponsaveisAtual  = Responsavel::where('imo_id', '=', $imo_id)->get()->toArray();

            if($ResponsaveisAtual) {
                $Aux = array();
                foreach ($ResponsaveisAtual as $Array) {
                    $Aux[$Array['id']] = $Array['id'];
                }
                $ResponsaveisAtual = $Aux;
            }



            $Responsaveis = array();

            foreach($input['responsavel'][$key] as $keyAux => $value) {


                if($value) { // Se exite o valor adiciona
                    if(isset($input['res_id'][$key][$keyAux])) $res_id     = $input['res_id'][$key][$keyAux]; else $res_id = false;
                    if(!$res_id) {
                        Responsavel::create([
                            'responsavel' => $value,
                            'email' => $input['email'][$key][$keyAux],
                            'imo_id' => $imo_id
                            ]);
                    } else {

                        unset($ResponsaveisAtual[$res_id]);
                        $Responsavel=Responsavel::find($res_id);
                        $Responsavel->update([
                            'responsavel' => $value,
                            'email' => $input['email'][$key][$keyAux],
                            ]);

                    }


                }
            }

            



        }//close foreach
        if($ImobiliariasAtual)
        {   
            Imobiliaria::destroy($ImobiliariasAtual);
        }
        if($ResponsaveisAtual)
        {   
            Responsavel::destroy($ResponsaveisAtual);
        }
    }// close class





}
