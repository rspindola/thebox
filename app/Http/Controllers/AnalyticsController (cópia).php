<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use DB;
use LaravelAnalytics;

use Carbon\Carbon;

use App\Http\Requests\ProjetosRequest;
use App\Projeto;

class AnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   

        // id data inicial e final
        $id     = $request->id;
//        $inicio = Carbon::createFromFormat('Y-m-d', $request->inicio);
//        $final  = Carbon::createFromFormat('Y-m-d', $request->final);

        $inicio = Carbon::createFromFormat('Y-m-d', '2016-04-1');
        $final  = Carbon::createFromFormat('Y-m-d', '2016-04-05');

        // pega o analytics do google
        $siteid = Projeto::select('analytics')->find($id);
        $siteid = 'ga:'.$siteid->analytics;


        // Grafico de Pizza
        $Dados['GraficoPizza'] = $this->GraficoPizza($id, $inicio, $final);

        $Graficos = array( 
            array('Grafico' => 'GraficoSessoes', 'Metrica' => 'ga:sessions', 'Arredondamento' => '0');
            array('Grafico' => 'GraficoUsuarios', 'Metrica' => 'ga:sessions', 'Arredondamento' => '0');
            array('Grafico' => 'GraficoVisualizacao', 'Metrica' => 'ga:sessions', 'Arredondamento' => '0');
            array('Grafico' => 'GraficoPaginasSessoes', 'Metrica' => 'ga:sessions', 'Arredondamento' => '0');
            array('Grafico' => 'GraficoTempoMedio', 'Metrica' => 'ga:sessions', 'Arredondamento' => '0');
            array('Grafico' => 'GraficoRejeicao', 'Metrica' => 'ga:sessions', 'Arredondamento' => '0');

        );


        // Grafico de Sessoes
        $Dados['GraficoSessoes'] = $this->GraficoSessoes($siteid, $inicio, $final);



        // Grafico de Usuarios
        $Dados['GraficoUsuarios'] = $this->GraficoUsuarios($siteid, $inicio, $final);

        // Grafico de Visualizacao
        $Dados['GraficoVisualizacao'] =$this->GraficoVisualizacao($siteid, $inicio, $final);

        // Grafico de sessao pagina
        $Dados['GraficoPaginasSessoes'] = $this->GraficoPaginasSessoes($siteid, $inicio, $final);

        // Grafico de sessao pagina
        $Dados['GraficoTempoMedio'] = $this->GraficoTempoMedio($siteid, $inicio, $final);

        // Grafico de Rejeicao
        $Dados['GraficoRejeicao'] = $this->GraficoRejeicao($siteid, $inicio, $final);





		//$analyticsData = LaravelAnalytics::setSiteId($siteid)->getVisitorsAndPageViews(7);
        return $Dados;
    }


    public function GraficoPizza($id, $inicio, $final) {

        $Array = DB::table('pessoas')
                 ->select('origem as label', DB::raw('count(*) as value'))
                 ->where('pro_id', $id)
                 ->where('created_at', '>=', $inicio)
                 ->where('created_at', '<=', $final)
                 ->groupBy('origem')
                 ->get();


        $cores = array(
            array('#F7464A', '#FF5A5E'), 
            array('#46BFBD', '#5AD3D1'), 
            array('#FDB45C', '#FFC870'), 
            array('#949FB1', '#A8B3C5'), 
            array('#F7464A', '#FF5A5E'),
            array('#F7464A', '#FF5A5E'), 
            array('#46BFBD', '#5AD3D1'), 
            array('#FDB45C', '#FFC870'), 
            array('#949FB1', '#A8B3C5'), 
            array('#F7464A', '#FF5A5E'),
            array('#F7464A', '#FF5A5E'), 
            array('#46BFBD', '#5AD3D1'), 
            array('#FDB45C', '#FFC870'), 
            array('#949FB1', '#A8B3C5'), 
            array('#F7464A', '#FF5A5E'),
            array('#F7464A', '#FF5A5E'), 
            array('#46BFBD', '#5AD3D1'), 
            array('#FDB45C', '#FFC870'), 
            array('#949FB1', '#A8B3C5'), 
            array('#F7464A', '#FF5A5E')
            );

        $Return = array();
        $i = 0;
        foreach ($Array as $key => $Aux) {
            $Return[$key]['label'] = $Aux->label;
            $Return[$key]['value'] = $Aux->value;
            $Return[$key]['color'] = $cores[$i][0];
            $Return[$key]['highlight'] = $cores[$i][1];
            $i++;
        }

        return $Return;

    }

    public function GraficoSessoes($id, $inicio, $final)
    {
        $Dados = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, 'ga:sessions', ['dimensions' => 'ga:date']);


        $Labels = '';
        $Data   = '';

        $Labels = array();
        $Array   = array();
        foreach ($Dados['rows'] as $value) {
            $Data    = substr($value[0], -2).'/'.substr($value[0], 4, -2).'/'.substr($value[0], 0, 4);
            $Labels[] = $Data;
            $Array[]   = $value[1];

        }
        $Array  = array('Label' => $Labels, 'Data' => $Array); 

        return $Array;

    }

    public function GraficoUsuarios($id, $inicio, $final)
    {
        $Dados = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, 'ga:users', ['dimensions' => 'ga:date']);


        $Labels = '';
        $Data   = '';

        $Labels = array();
        $Array   = array();
        foreach ($Dados['rows'] as $value) {
            $Data    = substr($value[0], -2).'/'.substr($value[0], 4, -2).'/'.substr($value[0], 0, 4);
            $Labels[] = $Data;
            $Array[]   = $value[1];

        }
        $Array  = array('Label' => $Labels, 'Data' => $Array); 

        return $Array;

    }
    public function GraficoVisualizacao($id, $inicio, $final)
    {
        $Dados = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, 'ga:pageviews', ['dimensions' => 'ga:date']);


        $Labels = '';
        $Data   = '';

        $Labels = array();
        $Array   = array();
        foreach ($Dados['rows'] as $value) {
            $Data    = substr($value[0], -2).'/'.substr($value[0], 4, -2).'/'.substr($value[0], 0, 4);
            $Labels[] = $Data;
            $Array[]   = $value[1];

        }
        $Array  = array('Label' => $Labels, 'Data' => $Array); 

        return $Array;

    }

    public function GraficoPaginasSessoes($id, $inicio, $final)
    {
        $Dados = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, 'ga:pageviewsPerSession', ['dimensions' => 'ga:date']);

        $Labels = '';
        $Data   = '';

        $Labels = array();
        $Array   = array();
        foreach ($Dados['rows'] as $value) {
            $Data       = substr($value[0], -2).'/'.substr($value[0], 4, -2).'/'.substr($value[0], 0, 4);
            $Labels[]   = $Data;
            $Array[]    = round($value[1], 2);

        }
        $Array  = array('Label' => $Labels, 'Data' => $Array); 

        return $Array;

    }


    public function GraficoTempoMedio($id, $inicio, $final)
    {
        $Dados = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, 'ga:avgSessionDuration', ['dimensions' => 'ga:date']);

        $Labels = '';
        $Data   = '';


        $Labels = array();
        $Array   = array();
        foreach ($Dados['rows'] as $value) {
            $Data       = substr($value[0], -2).'/'.substr($value[0], 4, -2).'/'.substr($value[0], 0, 4);
            $Labels[]   = $Data;
            $Array[]    = round($value[1]);

        }
        $Array  = array('Label' => $Labels, 'Data' => $Array); 

        return $Array;

    }


    public function GraficoRejeicao($id, $inicio, $final)
    {
        $Dados = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, 'ga:bounceRate', ['dimensions' => 'ga:date']);

        $Labels = '';
        $Data   = '';

        $Labels = array();
        $Array   = array();
        foreach ($Dados['rows'] as $value) {
            $Data       = substr($value[0], -2).'/'.substr($value[0], 4, -2).'/'.substr($value[0], 0, 4);
            $Labels[]   = $Data;
            $Array[]    = round($value[1], 2);

        }
        $Array  = array('Label' => $Labels, 'Data' => $Array); 

        return $Array;

    }


}
