<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;




use App\Http\Requests\ProjetosRequest;
use App\Projeto;
use App\Leads;
use App\LeadsValores;



class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id)
    {

        $Dados  = Projeto::find($id)->leads()->get();
        $pro_id = $id;
        return view('leads.index',compact(['Dados', 'id']));
    }


    public function edit($pro_id, $id)
    {

        $Leads      = Leads::select('origem')->find($id);
        $Dados      = Leads::find($id)->valores()->get();
        return view('leads.edit',compact(['pro_id', 'id', 'Leads', 'Dados']));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pro_id, $lea_id)
    {


//        $Projeto = Projeto::create($request->all());

//        $this->Salvar($request);
        $input = $request->input();
        LeadsValores::where('lea_id', '=', $lea_id)->delete();

        foreach($input['inicio'] as $key => $Inicio) {
            $valores        = LeadsValores::create([
                'inicio'    => $Inicio,
                'fim'       => $input['fim'][$key],
                'investido' => $input['investido'][$key],
                'meta'      => $input['meta'][$key],
                'lea_id'    => $lea_id
                ]);

        }//close foreach

        return redirect()->route('leads.index', $request->pro_id);
        ;

    }


    public function destroy($id)
    {

        Leads::find($id)->delete();
        return redirect()->back();
    }


    public function excel(request $request)
    {

        // Filtros de Datas
        // E necessario formatar a data por causa do analytics e como o analytics é a funcao mais usada
        $Inicio = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['inicio'] . ' 00:00:00');
        $Final  = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['final'] . ' 23:59:59');




        // Total
        $Dados          = Projeto::find($request['pro_id'])->pessoas()->where('created_at', '>=', $Inicio)->where('created_at', '<=', $Final)->get();


        $Excel = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><style>.Texto {mso-number-format:"\@"}</style><table>';
        $Excel.= '<tr><td class="Texto">nome</td><td class="Texto">email</td><td class="Texto">telefone</td><td class="Texto">mensagem</td><td class="Texto">coords</td><td class="Texto">origem</td><td class="Texto">peca</td><td class="Texto">campaign</td><td class="Texto">content</td><td class="Texto">term</td><td class="Texto">sistema</td><td class="Texto">Data</td></tr>';

        foreach ($Dados as $row) {

            $Excel.= '<tr>
            <td class="Texto">'.$row['nome'].'</td>
            <td class="Texto">'.$row['email'].'</td>
            <td class="Texto">'.$row['telefone'].'</td>
            <td class="Texto">'.$row['mensagem'].'</td>
            <td class="Texto">'.$row['coords'].'</td>
            <td class="Texto">'.$row['origem'].'</td>
            <td class="Texto">'.$row['peca'].'</td>
            <td class="Texto">'.$row['campaign'].'</td>
            <td class="Texto">'.$row['content'].'</td>
            <td class="Texto">'.$row['term'].'</td>
            <td class="Texto">'.$row['sistema'].'</td>
            <td class="Texto">'.$row['created_at'].'</td>
        </tr>';
    }

        // Configurações header para forçar o download
    header ("Expires: Mon, 8 Apl 2014 05:00:00 GMT");
    header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    header ("Content-type: application/x-msexcel; charset=utf-8");
    header ("Content-Disposition: attachment; filename=\"Dados.xls\"" );
    header ("Content-Description: Planilha" );

        // Envia o conteúdo do arquivo
    echo $Excel;

}



}
