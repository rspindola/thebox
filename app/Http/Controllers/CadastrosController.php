<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Pessoas;
use App\Projeto;

use Illuminate\Http\Request;

class CadastrosController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index($id) {
		$Inicial = \Carbon\Carbon::now()->subMonth()->format('Y-m-d');
		$Final   = \Carbon\Carbon::now()->format('Y-m-d');

		$Dados = Projeto::get()->find($id);

		return view('cadastros.index', compact(['Dados', 'id', 'Inicial', 'Final']));
	}

	public function dados(request $request, $id) {
		return view('home');
		// Filtros de Datas
		// E necessario formatar a data por causa do analytics e como o analytics é a funcao mais usada
		$Inicio = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['inicio'].' 00:00:00');
		$Final  = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['final'].' 23:59:59');

		// Pagina para ser exibida
		$Start  = $request['start'];
		$length = $request['length'];

		$Colunas               = array('nome', 'email', 'origem', 'peca', 'telefone', 'created_at', 'mensagem', 'nome', 'nome');
		$Coluna                = $request['order']['0']['column'];
		$Ordem                 = $request['order']['0']['dir'];
		if (!$Coluna) {$Coluna = '0';}

		// Total
		$Dados = Projeto::find($id)->pessoas()->select('id', 'origem', 'peca', 'nome', 'email', 'telefone', 'created_at', 'Mensagem', 'res_id')->with('responsavel', 'responsavel.imobiliaria');
		$Dados
			->where('created_at', '>=', $Inicio)
			->where('created_at', '<=', $Final);

		// Busca
		//        $Search = $request['search']['value'];
		$Search = $request['search%5Bvalue%5D'];
		if (strlen($Search) > 0) {
			$Search = '%'.$Search.'%';
			$Dados
				->where('peca', 'LIKE', $Search)
				->orWhere('nome', 'LIKE', $Search)
				->orWhere('telefone', 'LIKE', $Search)
				->orWhere('Mensagem', 'LIKE', $Search);
		}

		$Total = $Dados->count();

		// Total na tabela
		$Data = array('recordsTotal' => $Total, 'recordsFiltered' => $Total);

		// Seleciona os responsaveis

		// Busca o Responsavel

		// $Dados = $Dados->limit($length)->offset($Start)->orderBy($Colunas[$Coluna], $Ordem)->get();
		$Dados = $Dados->limit($length)->offset($Start)->orderBy('created_at', 'DESC')->get();

		foreach ($Dados as $row) {
			$id = $row['id'];

			// Condição para exibição do nome
			if ($row['nome'] != '') {$nome = strip_tags($row['nome']);
			} else {
				$nome = "<span class='tx-disable'>Não informado</span>";
			}

			// Condição para exibição da peça
			if ($row['origem'] != 'Desconhecido') {$origem = strip_tags($row['origem']);
			} else {
				$origem = '<span class="tx-disable">'.strip_tags($row['origem']).'</span>';
			}

			// Condição para exibição da peça
			if ($row['peca'] != 'Desconhecido') {$peca = strip_tags($row['peca']);
			} else {
				$peca = "<span class='tx-disable'>Desconhecido</span>";
			}

			// Condição para exibição da peça
			if ($row['telefone'] != '') {$telefone = strip_tags($row['telefone']);
			} else {
				$telefone = "<span class='tx-disable'>Não informado</span>";
			}

			// Condição para exibição das mensagens
			if ($row['Mensagem'] != '') {$mensagem = strip_tags($row['Mensagem']);
			} else {
				$mensagem = "<span class='tx-disable'>Este contato não possuí mensagem.</div>";
			}

			// Condição para exibição do responsável
			if ($row['responsavel']['responsavel']) {
				$Encaminhado = $row['responsavel']['imobiliaria']['imobiliaria'].'<br>'.$row['responsavel']['responsavel'].'<br>'.$row['responsavel']['email'];
			} else {
				$Encaminhado = "<span class='tx-disable'>Dados indisponíveis</div>";
			}

			if ($row['email'] != '') {$email = strip_tags($row['email']);
			} else {
				$email = "<span class='tx-disable'>Não informado</div>";
			}
			// <a href="#" onclick="event.preventDefault();" class="ModalEncaminhado" id='.$id.'><i class="fa fa-user"></i> Encaminhado para</a><input type="hidden" name="modal" class="ModalEncaminhado'.$id.'" value="'.$Encaminhado.'">

			$Data['data'][] = array(
				$nome,
				$email,
				$origem,
				$peca,
				$telefone,
				$row['created_at']->format('d/m/Y H:i'),
				'<a href="#" onclick="event.preventDefault();" class="ModalMensagem" id='.$id.'><i class="fa fa-eye"></i> Visualizar</a><input type="hidden" name="modal" class="ModalMensagem'.$id.'" value="'.strip_tags($mensagem).'">',
				'<a href="#" onclick="event.preventDefault();" class="ModalEncaminhado" id='.$id.'><i class="fa fa-user"></i> '.$row['responsavel']['responsavel'].'</a><input type="hidden" name="modal" class="ModalEncaminhado'.$id.'" value="'.$Encaminhado.'">',
				'<a href="'.url().'/cadastros/'.$id.'/destroy" class="btn btn-delete btn-xs"><i class="fa fa fa-trash"></i></a>',
			);
		}
		if (!isset($Data['data'])) {$Data['data'][] = array('', '', '', '', '', '', '', '', '');
		}

		return $Data;
	}

	public function excel(request $request, $id) {

		// Filtros de Datas
		// E necessario formatar a data por causa do analytics e como o analytics é a funcao mais usada
		$Inicio = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['inicio'].' 00:00:00');
		$Final  = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['final'].' 23:59:59');

		// Total
		//$Dados = Projeto::find($id)->pessoas()->where('created_at', '>=', $Inicio)->where('created_at', '<=', $Final)->get();

		//Ordenando por data
		$Dados = Projeto::find($id)->pessoas()->where('created_at', '>=', $Inicio)->where('created_at', '<=', $Final)->orderBy('created_at', 'DESC')->get();

		$Excel = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><style>.Texto {mso-number-format:"\@"}</style><table>';
		$Excel .= '<tr><td class="Texto">nome</td><td class="Texto">email</td><td class="Texto">telefone</td><td class="Texto">mensagem</td><td class="Texto">coords</td><td class="Texto">origem</td><td class="Texto">peça</td><td class="Texto">campaign</td><td class="Texto">content</td><td class="Texto">term</td><td class="Texto">sistema</td><td class="Texto">Encaminhamento</td><td class="Texto">Data</td></tr>';


		foreach ($Dados as $row) {

			if ($row['responsavel']['responsavel']) {
				$EncaminhadoMail = $row['responsavel']['email'];
			} else {
				$EncaminhadoMail = "Insisponível";
			}

			$Excel .= '<tr>
            <td class="Texto">'.$row['nome'].'</td>
            <td class="Texto">'.$row['email'].'</td>
            <td class="Texto">'.$row['telefone'].'</td>
            <td class="Texto">'.$row['mensagem'].'</td>
            <td class="Texto">'.$row['coords'].'</td>
            <td class="Texto">'.$row['origem'].'</td>
            <td class="Texto">'.$row['peca'].'</td>
            <td class="Texto">'.$row['campaign'].'</td>
            <td class="Texto">'.$row['content'].'</td>
            <td class="Texto">'.$row['term'].'</td>
            <td class="Texto">'.$row['sistema'].'</td>
						<td class="Texto">'.$EncaminhadoMail.'</td>
            <td class="Texto">'.$row['created_at']->format('d/m/Y H:i:s').'</td>
        </tr>';
		}

		// Configurações header para forçar o download
		header("Expires: Mon, 8 Apl 2014 05:00:00 GMT");
		header("Last-Modified: ".gmdate("D,d M Y H:i:s")." GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header("Content-type: application/x-msexcel; charset=utf-8");
		header("Content-Disposition: attachment; filename=\"Dados.xls\"");
		header("Content-Description: Planilha");

		// Envia o conteúdo do arquivo
		echo $Excel;

	}

	public function destroy($id) {

		Pessoas::find($id)->delete();
		return redirect()->back();
	}

}
