<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosRequest;
use App\Projeto;
use App\Calltracker;
use App\ProjetoTelefone;
use ZipArchive;
use Response;
use File;

class CalltrackerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */




    public function destroy($id)
    {
        Calltracker::find($id)->delete();
        return redirect()->back();
    }




    public function index($id)
    {


        $this->Salvar();
        // Formata data para enviar para o input
        $Inicial    = \Carbon\Carbon::now()->subMonth()->format('Y-m-d');
        $Final      = \Carbon\Carbon::now()->format('Y-m-d');

        $Dados      = Projeto::get()->find($id);
        $Calltracker= Projeto::find($id)->telefone()->count();
        return dd($Calltracker);
        
        return view('calltracker.index',compact(['Dados', 'id', 'Inicial', 'Final', 'Calltracker']));

    }


    public function dados(request $request, $id)
    {


        // Filtros de Datas
        // E necessario formatar a data por causa do analytics e como o analytics é a funcao mais usada
        $Inicio = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['inicio'] . ' 00:00:00');
        $Final  = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['final'] . ' 23:59:59');

        // Pagina para ser exibida
        $Start  = $request['start'];
        $length = $request['length'];
        $Colunas= array('origem', 'rota', 'telefone', 'data', 'data', 'duracao', 'audio');
        $Coluna = $request['order']['0']['column'];
        $Ordem  = $request['order']['0']['dir'];
        if(!$Coluna) $Coluna = '0';


        $Dados  = Projeto::find($id)->Calltracker()->select('id', 'origem', 'rota', 'telefone', 'duracao', 'audio', 'pro_id', 'data');

        $Dados
        ->where('data', '>=', $Inicio)
        ->where('data', '<=', $Final);


        // Busca
//        $Search = $request['search']['value'];
        $Search = $request['search%5Bvalue%5D'];
        if(strlen($Search) > 0)
        {
            $Search = '%'.$Search.'%';
            $Dados
            ->where('origem', 'LIKE', $Search)
            ->orWhere('telefone', 'LIKE', $Search)
            ->orWhere('data', 'LIKE', $Search);
        }


        $Total          = $Dados->count();

        // Total na tabela
        $Data = array('recordsTotal' => $Total, 'recordsFiltered' => $Total);

        // $Dados          = $Dados->limit($length)->offset($Start)->orderBy($Colunas[$Coluna], $Ordem)->get();
        $Dados          = $Dados->limit($length)->offset($Start)->orderBy('data', 'ASC')->get();

        foreach ($Dados as $row) {
            $Aux = explode(' ', $row['data']);
            if(isset($row['audio'])) $audio = '<a href="'.$row['audio'] .'" target="_blank" download><i class="fa fa-download"></i> Baixar mp3</a>'; else $audio = '';

            $Data['data'][] = array(
                $row['origem'],
                $row['rota'],
                $row['telefone'],
                $Aux[0],
                $Aux[1],
                $row['duracao'],
                $audio,
                '<a href="'.url().'/calltracker/'.$row['id'].'/destroy" class="btn btn-delete btn-xs"><i class="fa fa fa-trash"></i></a>'
                );

        }
        if(!isset($Data['data'])) $Data['data'][] = array('Não foi encontrado nenhum dado', '', '', '', '', '', '');
        return $Data;

    }

    public function Salvar()
    {


        // Array que cria os filtros
        $Filtros = array();

        // Filtros de Datas
        // Ultima data baixada
        $Data   = Calltracker::select('data')->orderBy('data', 'desc')->skip(0)->take(1)->get()->toArray();
        if(!isset($Data['0']['data'])) $Data['0']['data'] = '01/01/2014 01:00:50';
        $Filtros['start_date']  = \Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $Data['0']['data'])->addDay()->format('Y-m-d H:i:s');


        $Filtros['end_date']    = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        


        $Aux        = ProjetoTelefone::get();
        $Telefones  = array();
        foreach ($Aux as $Array) {
            $Telefones[$Array->telefone] = $Array->pro_id;
        }

        // Dados da apis
        $access_key = 'a44850dadb7da9cac1d4c3370a2c9044ed3712d';
        $secret_key = 'fe221f55c30fe290bf2900b4a6835a7335f1';
        $Page       = 1;
        for ($i=0; $i < 1000; $i++) {
            $Filtros['page']  = $Page;

            $ch      = curl_init('https://api.calltrackingmetrics.com/api/v1/accounts/44850/calls.json?'.http_build_query($Filtros));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_USERPWD, $access_key . ':' . $secret_key);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $Dados = curl_exec($ch);
            curl_close($ch);

            $Dados = json_decode($Dados);



            if(!$Dados->calls) { break;}
            $Dados  = $Dados->calls;

            foreach ($Dados as $key => $Array) {
                
                if(isset($Array->business_number_format)) {
                    $exp = explode('-', $Array->business_number_format);
                    $tel = $exp['1'].' '.$exp['2'].'-'.$exp['3'];

                    $exp            = explode('-', $Array->tracking_number_format);
                    $TelComparar    = $exp['1'].' '.$exp['2'].'-'.$exp['3'];
                    
                    if(isset($Telefones[$TelComparar])) {

                        if(isset($Array->audio)) $Audio = $Array->audio; else $Audio = NULL;


                        if(isset($Array->caller_number_split[3])) { $Numero = '('. $Array->caller_number_split[1] .') '. $Array->caller_number_split[2] .'-'. $Array->caller_number_split[3]; } else { $Numero = $Array->caller_number_format; }

                        $Criado = \Carbon\Carbon::parse($Array->called_at)->format('Y-m-d H:i:s');
                        $Data = array(
                            'origem' => $Array->source,
                            'telefone' => $Numero,
                            'data' => $Criado,
                            'duracao' => $Array->duration .' seg',
                            'audio' => $Audio,
                            'rota' => $tel,
                            'pro_id' => $Telefones[$TelComparar]
                            );
                        Calltracker::updateOrCreate($Data);
                    }
                }

            }
            $Page++;
        }


    }


    public function excel(request $request, $id)
    {

        // E necessario formatar a data por causa do analytics e como o analytics é a funcao mais usada
        $Inicio = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['inicio'] . ' 00:00:00');
        $Final  = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['final'] . ' 23:59:59');



        $Dados  = Projeto::find($id)->Calltracker()->select('origem', 'telefone', 'data', 'duracao')
        ->where('data', '>=', $Inicio)
        ->where('data', '<=', $Final);




        $Excel = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><style>.Texto {mso-number-format:"\@"}</style><table>';
        $Excel.= '<tr><td class="Texto">Origem</td><td class="Texto">Telefone</td><td class="Texto">Data</td><td class="Texto">Horário</td><td class="Texto">Duração da Chamada</td></tr>';
        if($Dados->count()) {
            $Dados = $Dados->get();
            foreach ($Dados as $key => $Array) {
                $Aux = explode(' ', $Array->data);

                if(isset($Array->caller_number_split[3])){ $Numero = '('. $Array->caller_number_split[1] .') '. $Array->caller_number_split[2] .'-'. $Array->caller_number_split[3]; } else { $Numero = $Array->caller_number_format; }
                $Excel.= '<tr>
                <td class="Texto">'.$Array->origem.'</td>
                <td class="Texto">'.$Array->telefone.'</td>
                <td class="Texto">'.$Aux[0].'</td>
                <td class="Texto">'.$Aux[1].'</td>
                <td class="Texto">'.$Array->duracao.'</td>
            </tr>';
        }
    }




            // Configurações header para forçar o download
    header ("Expires: Mon, 8 Apl 2014 05:00:00 GMT");
    header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    header ("Content-type: application/x-msexcel; charset=utf-8");
    header ("Content-Disposition: attachment; filename=\"Calltracker.xls\"" );
    header ("Content-Description: Planilha" );

        // Envia o conteúdo do arquivo
    echo $Excel;

}

public function audio(request $request, $id) {


        // Filtros de Datas
        // E necessario formatar a data por causa do analytics e como o analytics é a funcao mais usada
    $Inicio = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['inicio'] . ' 00:00:00');
    $Final  = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request['final'] . ' 23:59:59');


    $Aux  = Projeto::get()->find($id);
    $Projeto = $Aux->projeto;

    $Arquivo = $Projeto.'.zip';
    $Destino = public_path() . '/' . $Arquivo;
    File::delete($Destino);

            // Create "MyCoolName.zip" file in public directory of project.
    $zip = new ZipArchive;

    if($zip->open($Destino, ZipArchive::CREATE) === true)
    {


        $Arquivos  = Projeto::find($id)->Calltracker()->select('telefone', 'audio', 'data')->whereNotNull('audio')
        ->where('data', '>=', $Inicio)
        ->where('data', '<=', $Final);

        if($Arquivos->count()) {
            $Arquivos  = $Arquivos->get();
            
            foreach($Arquivos as $Array) {
                $Download = $this->curl_get_contents($Array->audio);
                $Data = \Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $Array->data)->addDay()->format('d-m-Y');
                $zip->addFromString(basename($Projeto .' '. $Array->telefone .' '. $Data .'.mp3'),$Download);

            }
        }


        $zip->close();

        $headers = array(
            'Content-Type' => 'application/octet-stream',
            );

            // Download .zip file.
        return Response::download($Destino, $Arquivo, $headers );





    }

}

public function curl_get_contents($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}


}
