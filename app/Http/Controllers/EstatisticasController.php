<?php

namespace App\Http\Controllers;

use App\Calltracker;

use App\Http\Controllers\Controller;
use App\Leads;
use App\Pessoas;
use App\Projeto;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use LaravelAnalytics;

class EstatisticasController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id) {

		$Inicial = \Carbon\Carbon::now()->subMonth()->format('Y-m-d');
		$Final   = \Carbon\Carbon::now()->format('Y-m-d');
		$Dados   = Projeto::get()->find($id);

		return view('estatisticas.index', compact('id', 'Dados', 'Inicial', 'Final'));

	}

	/**
	 * Cria o datatable das origens
	 */

	public function datatable(request $request, $id) {

		// Filtros de Datas
		// E necessario formatar a data por causa do analytics e como o analytics é a funcao mais usada
		$PerInicio  = Carbon::createFromFormat('Y-m-d H:i:s', $request['inicio'].' 00:00:00');
		$PerFinal   = Carbon::createFromFormat('Y-m-d H:i:s', $request['final'].' 23:59:59');
		$PerQtdDias = $PerInicio->diff($PerFinal)->days;
		$PerQtdDias++;

		// Pagina para ser exibida
		$Start  = $request['start'];
		$length = $request['length'];

		// Busca as origens
		$Leads = Projeto::find($id)->leads();

		// Total de origens
		$Total = $Leads->count();
		$Data  = array('recordsTotal' => $Total, 'recordsFiltered' => $Total);

		// Busca
		//        $Search = $request['search']['value'];
		$Search = $request['search%5Bvalue%5D'];
		if (strlen($Search) > 0) {
			$Search = '%'.$Search.'%';
			$Leads
				->where('origem', 'LIKE', $Search);
		}

		// Pega os dados dados da origens
		$Dados = $Leads->limit($length)->offset($Start)->get();

		// Foreach com as origens
		foreach ($Dados as $Origem) {

			// Seleciona os valores para o perido escolhido
			$LeadsValores = db::table('leads_valores')
				->where('lea_id', '=', $Origem['id'])
				->where('inicio', '<=', $PerFinal)
				->where('fim', '>=', $PerInicio)	->get();

			// Se o estatisticas nao e do calltracker
			if ($Origem['origem'] != 'Calltracker') {
				// Seleciona a quantidade de pessoas no periodo
				$Pessoas = Pessoas::
				where('pro_id', '=', $id)
					->where('Origem', '=', $Origem['origem'])
					->where('created_at', '>=', $PerInicio)
					->where('created_at', '<=', $PerFinal);

				// Total de pessoas no cadastro
				$Total = 0;
				$Total = $Pessoas->count();

				// Cria a Tabela de Midias
				$Pessoas = $Pessoas->groupBy('peca')->select('peca', DB::raw('count(*) as total'))->get();
				$Tabela  = "<table class='table table-striped nb-header text-left'><thead><tr><th>Mídia</th><th>Total</th></tr></thead><tbody>";

				foreach ($Pessoas as $Array) {
					$Tabela .= '<tr>';
					$Tabela .= '<td>'.$Array['peca'].'</td>';
					$Tabela .= '<td>'.$Array['total'].'</td>';
					$Tabela .= '</tr>';
				}
				$Tabela .= '</tbody></table>';
			} else {
				// Estatisticas do Calltracker
				$Total  = Calltracker::where('pro_id', '=', $id)->where('data', '>=', $PerInicio)->where('data', '<=', $PerFinal)->count();
				$Tabela = '';

			}

			// Variveis utilizadas no calculos
			$Investido = 0;
			$Final     = 0;
			$Meta      = 0;
			$Variacao  = 0;
			$Qtd       = 0;

			// Foreach com os dados do leads no periodo
			foreach ($LeadsValores as $key => $ArrayValores) {

				// Cria as datas e calcula a diferenca entre elas
				$LeadInicio = Carbon::createFromFormat('Y-m-d H:i:s', $ArrayValores->inicio.' 00:00:00');
				$LeadFinal  = Carbon::createFromFormat('Y-m-d H:i:s', $ArrayValores->fim.' 00:00:00')->addDays(1);
				$QtdDias    = $LeadInicio->diff($LeadFinal)->days;

				// Calculos de valor investido e meta
				// Adicionada condição para se a data selecionada for maior que o perio evitando adicionar valores que não existem
				// $Investido = $Investido+(($ArrayValores->investido/$QtdDias)*$PerQtdDias);
				if ($PerQtdDias < $QtdDias) {
					$Investido = $Investido+(($ArrayValores->investido/$QtdDias)*$PerQtdDias);
				} else {
					$Investido = $Investido+(($ArrayValores->investido));
				}
				$Meta = $Meta+$ArrayValores->meta;
				$Qtd++;
			}

			// Formata os dados caso nao tenha dados aparece como nao ha dados
			if (!$Investido) {
				$Investido = 'Não há dados';
			} else {
				$Meta = $Meta/$Qtd;
				// Caso nao tenha o total de pessoas
				if ($Total) {$Final = $Investido/$Total;
				} else {
					$Final = 0;
				}

				$Variacao  = number_format(round($Meta-$Final, 2), 2, ",", ".");
				$Final     = number_format(round($Final, 2), 2, ",", ".");
				$Meta      = number_format(round($Meta, 2), 2, ",", ".");
				$Investido = number_format(round($Investido, 2), 2, ",", ".");

				$FinalFormat = floatval(str_replace(",", ".", $Final));
				$MetaFormat  = floatval(str_replace(",", ".", $Meta));
			}

			/* Formatação variação */
			if ($Variacao == 0) {$ValVariacao = '<span class="tx-disable">R$'.$Variacao.'</span>';} else {
				if ($Variacao > 0) {
					$ValVariacao = 'R$'.$Variacao;
				} else {
					// $ValVariacao = '-R$'.abs($Variacao);
					$ValVariacao = '-R$'.number_format(abs($Variacao), 2, ',', '');
				}
			}

			/* Formatação valor final */
			if ($Final == 0) {$ValFinal = '<span class="tx-disable">R$'.$Final.'</span>';} else {
				if ($FinalFormat <= $MetaFormat) {
					$ValFinal = '<span class="tx-green">R$'.$Final.'</span>';
				} else {
					$ValFinal = '<span class="tx-red">R$'.$Final.'</span>';
				}
			}

			/* Formatação valor Meta */
			if ($Meta == 0) {$ValMeta = '<span class="tx-disable">R$'.$Meta.'</span>';} else { $ValMeta = 'R$'.$Meta;}

			/* Formatação valor investido */
			if ($Investido == 'Não há dados') {$ValInvestido = '<span class="tx-disable">'.$Investido.'</span>';} else { $ValInvestido = 'R$'.$Investido;}

			// Cria o json que é retornado
			$Data['data'][] = array(
				$Origem['origem'],
				'<a href="#modal" onclick="event.preventDefault();" class="ModalMidia" id='.$Origem['id'].'><i class="fa fa-eye"></i> Visualizar mídias</a><input type="hidden" name="modal" class="ModalMidia'.$Origem['id'].'" value="'.$Tabela.'">',
				$Total,
				$ValInvestido,
				$ValFinal,
				$ValMeta,
				$ValVariacao,
				'',
			);

			// $Data['data'][] = array(
			// 	$Origem['origem'],
			// 	'<a href="#modal" onclick="event.preventDefault();" class="ModalMidia" id='.$Origem['id'].'><i class="fa fa-eye"></i> Visualizar mídias</a><input type="hidden" name="modal" class="ModalMidia'.$Origem['id'].'" value="'.$Tabela.'">',
			// 	$Total,
			// 	$Investido,
			// 	$Final,
			// 	$Meta,
			// 	$Variacao,
			// 	'',
			// );
		}

		if (!isset($Data['data'])) {$Data['data'][] = array('', '', '', '', '', '', '');
		}

		return $Data;

		$Dados
			->where('created_at', '>=', $Inicio)
			->where('created_at', '<=', $Final);

	}

	public function dados(Request $request) {

		// id data inicial e final
		$id = $request->id;

		$inicio = Carbon::createFromFormat('Y-m-d', $request->inicio);
		$final  = Carbon::createFromFormat('Y-m-d', $request->final);

		//        $inicio = Carbon::createFromFormat('Y-m-d', '2016-04-1');
		//        $final  = Carbon::createFromFormat('Y-m-d', '2016-04-05');

		// pega o analytics do google
		$Dados = Projeto::get()->find($id);

		$Metas  = $Dados->meta()->get();
		$siteid = 'ga:'.$Dados->analytics;

		// Grafico de Pizza
		$Dados['GraficoPizza'] = $this->GraficoPizza($id, $inicio, $final);

		$Graficos = array(
			array('Grafico' => 'GraficoSessoes', 'Metrica' => 'ga:sessions', 'Arredondamento' => '0', 'Divisao' => false),
			array('Grafico' => 'GraficoUsuarios', 'Metrica' => 'ga:users', 'Arredondamento' => '0', 'Divisao' => false),
			array('Grafico' => 'GraficoVisualizacao', 'Metrica' => 'ga:pageviews', 'Arredondamento' => '0', 'Divisao' => false),
			array('Grafico' => 'GraficoPaginasSessoes', 'Metrica' => 'ga:pageviewsPerSession', 'Arredondamento' => '2', 'Divisao' => false),
			array('Grafico' => 'GraficoTempoMedio', 'Metrica' => 'ga:avgSessionDuration', 'Arredondamento' => '0'),
			array('Grafico' => 'GraficoRejeicao', 'Metrica' => 'ga:bounceRate', 'Arredondamento' => '2'),
			array('Grafico' => 'GraficoNovasSessoes', 'Metrica' => 'ga:percentNewSessions', 'Arredondamento' => '2'),

		);

		$i          = 1;
		$LabelMetas = array();
		foreach ($Metas as $Array) {
			$LabelMetas[] = $Array['meta'];
			$Graficos[]   = array('Grafico' => 'GraficoMeta'.$i, 'Metrica' => 'ga:goal'.$i.'Starts', 'Arredondamento' => '0');
			$i++;
		}
		$Dados['LabelMetas'] = $LabelMetas;

		foreach ($Graficos as $Array) {
			$Dados[$Array['Grafico']] = $this->GraficoLinha($siteid, $inicio, $final, $Array['Metrica'], $Array['Arredondamento'], false);
		}

		$Dados['HeatMap'] = $this->HeatMap($id, $inicio, $final);
		return $Dados;
	}

	public function GraficoPizza($id, $inicio, $final) {

		$Array = DB::table('pessoas')
			->select('origem as label', DB::raw('count(*) as value'))
			->where('pro_id', $id)
			->where('created_at', '>=', $inicio)
			->where('created_at', '<=', $final)
			->groupBy('origem')
			->get();

		$Calltracker = DB::table('calltrackers')
			->where('pro_id', $id)
			->where('data', '>=', $inicio)
			->where('data', '<=', $final)
			->count();

		$cores = array(

			array('#3498db'),
			array('#9b59b6'),
			array('#34495e'),
			array('#16a085'),
			array('#27ae60'),
			array('#2980b9'),
			array('#8e44ad'),
			array('#2c3e50'),
			array('#f1c40f'),
			array('#e67e22'),
			array('#e74c3c'),
			array('#ecf0f1'),
			array('#95a5a6'),
			array('#f39c12'),
			array('#d35400'),
			array('#c0392b'),
			array('#bdc3c7'),
			array('#7f8c8d'),
			array('#1abc9c'),
			array('#2ecc71')
		);

		$Return = array();
		$i      = 0;
		foreach ($Array as $key => $Aux) {
			// $Return[$key]['label'] = $Aux->label;
			// $Return[$key]['value'] = $Aux->value;
			// $Return[$key]['color'] = $cores[$i][0];
			// $Return[$key]['highlight'] = $cores[$i][1];

			$Return['data'][$key]            = $Aux->value;
			$Return['backgroundColor'][$key] = $cores[$i][0];

			if ($Aux->label != '') {
				$Return['labels'][$key] = $Aux->label;
			} else {

				$Return['labels'][$key] = 'Desconhecido';
			}

			$i++;
		}
		if ($Calltracker > 0) {
			$Return['backgroundColor'][$i] = $cores[$i][0];
			$Return['data'][$i]            = $Calltracker;
			$Return['labels'][$i]          = 'Calltracker';
		}

		return $Return;

	}

	public function GraficoLinha($id, $inicio, $final, $Metrica, $Arredondamento, $Divisao) {

		$Dados = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, $Metrica, ['dimensions' => 'ga:date']);

		$Labels = '';
		$Data   = '';

		$Labels = array();
		$Array  = array();
		$Total  = 0;
		foreach ($Dados['rows'] as $value) {
			$Data     = substr($value[0], -2).'/'.substr($value[0], 4, -2).'/'.substr($value[0], 0, 4);
			$Labels[] = $Data;
			$Aux      = round($value[1], $Arredondamento);
			$Array[]  = $Aux;
			$Total    = $Total+$Aux;

		}
		if ($Metrica == 'ga:users') {
			$Aux   = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, $Metrica, []);
			$Total = round($Aux['totalsForAllResults']['ga:users'], 2);
		} else {
			$Total = round($Dados['totalsForAllResults'][$Metrica], $Arredondamento);
		}
		$Array = array('Label' => $Labels, 'Data' => $Array, 'Total' => $Total);

		return $Array;

	}

	public function HeatMap($id, $inicio, $final) {

		$Array = DB::table('pessoas')
			->select('coords')
			->where('pro_id', $id)
			->where('created_at', '>=', $inicio)
			->where('created_at', '<=', $final)
			->where('created_at', '<=', $final)
			->where('coords', '>', '')
			->whereNotNull('coords')
			->get();

		$Dados = array();

		foreach ($Array as $key => $Aux) {
			if (strlen($Aux->coords) > 10) {
				$Aux     = substr($Aux->coords, 0, -1);
				$Aux     = substr($Aux, 1);
				$Aux     = explode(',', $Aux);
				$Dados[] = array('lat' => $Aux[0], 'lng' => $Aux[1]);
			}
		}
		return $Dados;
	}

}
