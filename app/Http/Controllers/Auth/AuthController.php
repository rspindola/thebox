<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Input;


use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
/*    
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout', 'getRegister', 'postRegister']]);
    }
*/




    public function index()
    {
        $users = User::all();
        return view('auth.index',compact('users'));

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    public function create()
    {
        return view('auth.create');
    }


    protected function store(UserRequest $request)
    {
        $valores = $request->all();

        // Caso não exista senha nao altera o campo de senha
        if($valores['password']) {
            $valores['password'] = bcrypt($valores['password']);
        } else {
            unset($valores['password']);
        }
        $Usuario = User::create($valores);
        
        $this->Salvar($request, $Usuario->id);

        // Upload da Imagem
        if(isset($valores['image'])) {
            $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = $valores['email'].'.'.$extension; // renameing image
            Input::file('image')->move(public_path().'/img/Avatar', $fileName); // uploading file to given path

        }
        return redirect('usuarios');
    }


    public function edit($id)
    {
        $Dados=User::find($id);
        return view('auth.edit',compact('Dados'));
    }



    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect('usuarios');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->Salvar($request, $id);

        $valores=$request->all();
        // Caso nao tenha senha nao atualiza
        if($valores['password']) {
            $valores['password'] = bcrypt($valores['password']);
        } else {
            unset($valores['password']);
        }
        $User=User::find($id);
        $User->update($valores);

        if(isset($valores['image'])) {
            // Upload da Imagem
//            $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
//            $fileName = $valores['email'].'.'.$extension; // renameing image
            $fileName = $valores['email']; // renameing image
            Input::file('image')->move(public_path().'/img/Avatar', $fileName); // uploading file to given path

        }
        


        return redirect('usuarios');
    }
    public function Salvar(Request $request, $usu_id)
    {
        $input = $request->input();

        // Salva os Usuarios
        $projetos  = explode(',', $input['projeto']);
        User::find($usu_id)->projeto()->sync($projetos);

    }
    public function tag()
    {
        $Dados = User::select(['id', 'name'])->get();

        $Aux = array();
        foreach ($Dados as $Array) {
            $Aux[] = array('value' => $Array->id, 'text' => $Array->name);
        }
        return $Aux;
    }

    public function tagselecionadas($id)
    {

        $Dados = User::find($id)->projeto()->get();        
        $Aux = array();
        foreach ($Dados as $Array) {
            $Aux[] = array('value' => $Array->id, 'text' => $Array->projeto);
        }
        return $Aux;
    }

}
