<?php 

namespace App\Http\Controllers;

use App\Projeto;
use App\User;
use Auth;


class HomeController extends Controller {
    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {

        $id = Auth::id();
        $Dados = User::find($id)->projeto()->where('status', 1)->get();

        return view('home',compact('Dados'));
    }
}