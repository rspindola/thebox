<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use DB;
use LaravelAnalytics;

use Carbon\Carbon;

use App\Http\Requests\ProjetosRequest;
use App\Projeto;

class AnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function estatisticas($id)
    {

    $Dados          = Projeto::get()->find($id);
    return view('projetos.estatisticas',compact('Dados'));
    
    }

    public function index(Request $request)
    {   

        // id data inicial e final
        $id     = $request->id;
        $inicio = Carbon::createFromFormat('Y-m-d', $request->inicio);
        $final  = Carbon::createFromFormat('Y-m-d', $request->final);

//        $inicio = Carbon::createFromFormat('Y-m-d', '2016-04-1');
//        $final  = Carbon::createFromFormat('Y-m-d', '2016-04-05');

        // pega o analytics do google
        $siteid = Projeto::select('analytics')->find($id);
        $siteid = 'ga:'.$siteid->analytics;

        // Grafico de Pizza
        $Dados['GraficoPizza'] = $this->GraficoPizza($id, $inicio, $final);

        $Graficos = array( 
            array('Grafico' => 'GraficoSessoes', 'Metrica' => 'ga:sessions', 'Arredondamento' => '0'),
            array('Grafico' => 'GraficoUsuarios', 'Metrica' => 'ga:users', 'Arredondamento' => '0'),
            array('Grafico' => 'GraficoVisualizacao', 'Metrica' => 'ga:pageviews', 'Arredondamento' => '0'),
            array('Grafico' => 'GraficoPaginasSessoes', 'Metrica' => 'ga:pageviewsPerSession', 'Arredondamento' => '2'),
            array('Grafico' => 'GraficoTempoMedio', 'Metrica' => 'ga:avgSessionDuration', 'Arredondamento' => '0'),
            array('Grafico' => 'GraficoRejeicao', 'Metrica' => 'ga:bounceRate', 'Arredondamento' => '2'),
            array('Grafico' => 'GraficoNovasSessoes', 'Metrica' => 'ga:percentNewSessions', 'Arredondamento' => '2'),
            array('Grafico' => 'GraficoMeta1', 'Metrica' => 'ga:goal1Starts', 'Arredondamento' => '0'),
            array('Grafico' => 'GraficoMeta2', 'Metrica' => 'ga:goal2Starts', 'Arredondamento' => '0'),
            array('Grafico' => 'GraficoMeta3', 'Metrica' => 'ga:goal3Starts', 'Arredondamento' => '0'),
            array('Grafico' => 'GraficoMeta4', 'Metrica' => 'ga:goal4Starts', 'Arredondamento' => '0'),
            array('Grafico' => 'GraficoMeta5', 'Metrica' => 'ga:goal5Starts', 'Arredondamento' => '0')

            );
        
        foreach ($Graficos as $Array) {
            $Dados[$Array['Grafico']] = $this->GraficoLinha($siteid, $inicio, $final, $Array['Metrica'], $Array['Arredondamento']);
        }


        $Dados['HeatMap'] = $this->HeatMap($id, $inicio, $final);
        return $Dados;
    }


    public function GraficoPizza($id, $inicio, $final) {

        $Array = DB::table('pessoas')
        ->select('origem as label', DB::raw('count(*) as value'))
        ->where('pro_id', $id)
        ->where('created_at', '>=', $inicio)
        ->where('created_at', '<=', $final)
        ->groupBy('origem')
        ->get();


        $cores = array(
            array('#F7464A', '#FF5A5E'), 
            array('#46BFBD', '#5AD3D1'), 
            array('#FDB45C', '#FFC870'), 
            array('#949FB1', '#A8B3C5'), 
            array('#F7464A', '#FF5A5E'),
            array('#F7464A', '#FF5A5E'), 
            array('#46BFBD', '#5AD3D1'), 
            array('#FDB45C', '#FFC870'), 
            array('#949FB1', '#A8B3C5'), 
            array('#F7464A', '#FF5A5E'),
            array('#F7464A', '#FF5A5E'), 
            array('#46BFBD', '#5AD3D1'), 
            array('#FDB45C', '#FFC870'), 
            array('#949FB1', '#A8B3C5'), 
            array('#F7464A', '#FF5A5E'),
            array('#F7464A', '#FF5A5E'), 
            array('#46BFBD', '#5AD3D1'), 
            array('#FDB45C', '#FFC870'), 
            array('#949FB1', '#A8B3C5'), 
            array('#F7464A', '#FF5A5E')
            );

        $Return = array();
        $i = 0;
        foreach ($Array as $key => $Aux) {
            $Return[$key]['label'] = $Aux->label;
            $Return[$key]['value'] = $Aux->value;
            $Return[$key]['color'] = $cores[$i][0];
            $Return[$key]['highlight'] = $cores[$i][1];
            $i++;
        }

            // $Return['label'][$key] = $Aux->label;
            // $Return['value'][$key] = $Aux->value;
            // $Return['color'][$key] = $cores[$i][0];
            // $Return['highlight'][$key] = $cores[$i][1];

        return $Return;

    }



    public function GraficoLinha($id, $inicio, $final, $Metrica, $Arredondamento)
    {

        $Dados = LaravelAnalytics::setSiteId($id)->performQuery($inicio, $final, $Metrica, ['dimensions' => 'ga:date']);

        $Labels = '';
        $Data   = '';

        $Labels = array();
        $Array   = array();
        foreach ($Dados['rows'] as $value) {
            $Data       = substr($value[0], -2).'/'.substr($value[0], 4, -2).'/'.substr($value[0], 0, 4);
            $Labels[]   = $Data;
            $Array[]    = round($value[1], $Arredondamento);

        }
        $Array  = array('Label' => $Labels, 'Data' => $Array); 

        return $Array;

    }



    public function HeatMap($id, $inicio, $final) {

        $Array = DB::table('pessoas')
        ->select('coords')
        ->where('pro_id', $id)
        ->where('created_at', '>=', $inicio)
        ->where('created_at', '<=', $final)
        ->where('created_at', '<=', $final)
        ->where('coords', '>', '')
        ->whereNotNull('coords')
        ->get();
        
        $Dados = array();

        foreach ($Array as $key => $Aux) {
            if(strlen($Aux->coords) > 10) {
                $Aux     = substr($Aux->coords, 0, -1);
                $Aux     = substr($Aux, 1);
                $Aux     = explode(',', $Aux);
                $Dados[] = array('lat' => $Aux[0], 'lng' => $Aux[1]);
            }
        }
        return $Dados;
    }



}
