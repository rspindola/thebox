<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosRequest;
use App\Projeto;
use App\Leads;
use App\LeadsValores;
use App\Pessoas;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->beforeFilter('csrf', ['on' => '']);
    }

    public function store(Request $request)
    {
        $company = new Pessoas();
        $company->fill($request->all());
        return response()->json($company, 201);
        $company->save();
        

        return response()->json($company, 201);

    }

}
