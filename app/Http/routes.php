<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('/', ['as' =>'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::get('auth/login', ['as' =>'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' =>'login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);


Route::group(['middleware' => ['auth', 'cors']], function()
{

	Route::get('home', ['as' => 'home', 'uses' => 'HomeController@index']);
	
	// Registration routes...
	Route::get('usuarios/tag', ['as' => 'usuarios.tag', 'uses' => 'Auth\AuthController@tag']);
	Route::get('usuarios/{id}/tagselecionadas', ['as' => 'usuarios.tagselecionadas', 'uses' => 'Auth\AuthController@tagselecionadas']);
	
	// Password reset link request routes...
	Route::get('forgot', ['as' =>'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
	Route::post('forgot', ['as' =>'password/email', 'uses' => 'Auth\PasswordController@postEmail']);


	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset', 'Auth\PasswordController@postReset');

	// Projetos
	Route::get('projeto/tag', ['as' => 'projeto.tag', 'uses' => 'ProjetosController@tag']);
	Route::get('projeto/{id}/tagselecionadas', ['as' => 'projeto.tagselecionadas', 'uses' => 'ProjetosController@tagselecionadas']);
	Route::get('projeto/{id}/tagtelefones', ['as' => 'projeto.tagselecionadas', 'uses' => 'ProjetosController@tagtelefones']);
	Route::get('projeto/{id}/datatable', ['as' =>'projeto.dados', 'uses' => 'ProjetosController@dados']);


	// Verifica se e admin se for tem acesso ao projetos e usuarios
	Route::group(['middleware' => ['admin']], function()
	{
		Route::resource('projetos', 'ProjetosController');
		Route::resource('usuarios', 'Auth\AuthController');

	});

	Route::group(['middleware' => ['userProject']], function()
	{
	
	// Cadastros
	Route::get('projetos/{id}/cadastros', ['as' => 'cadastros.index', 'uses' => 'CadastrosController@index']);
	Route::get('cadastros/{id}/destroy', ['as' => 'cadastros.destroy', 'uses' => 'CadastrosController@destroy']);
	Route::get('cadastros/{id}/datatable', ['as' => 'cadastros.dados', 'uses' => 'CadastrosController@dados']);
	Route::post('cadastros/{id}/excel', ['as' =>'cadastros.excel', 'uses' => 'CadastrosController@excel']);

	// Estatisticas
	Route::get('projetos/{id}/estatisticas', ['as' => 'estatisticas.index', 'uses' => 'EstatisticasController@index']);
	Route::get('estatisticas/{id}', ['as' => 'estatisticas.dados', 'uses' => 'EstatisticasController@dados']);
	Route::get('estatisticas/{id}/datatable', ['as' => 'estatisticas.datatable', 'uses' => 'EstatisticasController@datatable']);

	// Calltracker
	Route::get('projetos/{id}/calltracker', ['as' =>'calltracker.index', 'uses' => 'CalltrackerController@index']);
	Route::get('calltracker/{id}/destroy', ['as' => 'calltracker.destroy', 'uses' => 'CalltrackerController@destroy']);
	Route::get('calltracker/{id}/datatable', ['as' =>'calltracker.dados', 'uses' => 'CalltrackerController@dados']);
	Route::post('calltracker/{id}/audio', ['as' =>'calltracker.audio', 'uses' => 'CalltrackerController@audio']);
	Route::post('calltracker/{id}/excel',  ['as' =>'calltracker.excel', 'uses' => 'CalltrackerController@excel']);

	// Leads
	Route::get('projetos/{id}/leads', ['as' => 'leads.index', 'uses' => 'LeadsController@index']);
	Route::get('projetos/{pro_id}/leads/{id}', ['as' => 'leads.edit', 'uses' => 'LeadsController@edit']);
	Route::patch('projetos/{pro_id}/leads/{id}/', ['as' =>'leads.update', 'uses' => 'LeadsController@update']);
	Route::delete('projetos/{id}/leads', ['as' => 'leads.destroy', 'uses' => 'LeadsController@destroy']);
	Route::post('leads', ['as' =>'leads.excel', 'uses' => 'LeadsController@excel']);

	});
});

Route::group(array('prefix' => 'api'), function()
{

  Route::get('/', function () {
      return response()->json(['message' => 'Jobs API', 'status' => 'Connected']);;
  });

  Route::resource('leads', 'Api\LeadsController');
});

