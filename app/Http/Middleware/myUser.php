<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Projeto;
use App\User;
use Auth;

class myUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        
    }



    public function handle($request, Closure $next)
    {

         $id = Auth::id();
         $projectId = $request->id;
         // $projetos  = User::find($id)->projeto()->where('status', 1)->get();

         $project = User::find($id)->projeto()->where('status', 1)->find($projectId);
       

        if ( $project || $this->auth->check() && $this->auth->user()->isAdmin())
        {
            return $next($request);

        }

        return redirect('home');

    }

}
