<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
	protected $fillable = array('nome' , 'email' , 'telefone' , 'mensagem', 'coords', 'origem' , 'peca' , 'term' , 'content' , 'campaign' , 'sistema' , 'created_at' , 'res_id' , 'pro_id');



    /**
	* Relacionamentos
	*/

	public function projeto()
	{
		return $this->belongsTo('App\Projeto');
	}

	public function valores()
	{
		return $this->hasMany('App\LeadsValores', 'lea_id');
	}

}