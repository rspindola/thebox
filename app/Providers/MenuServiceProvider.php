<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Projeto;
use App\User;
use Auth;


class MenuServiceProvider extends ServiceProvider
{


        /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {   

        view()->composer('layout', function ($view)
        {

            if(Auth::user()->isAdmin()) {
                $Menu = false;
            } else {
                $id = Auth::id();
                $Menu = User::find($id)->projeto()->where('status', 1)->get();
            }

            $view->with('Menu', $Menu);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
