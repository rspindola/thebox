<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imobiliaria extends Model
{
	protected $fillable = array('imobiliaria', 'pro_id');


	public function projeto()
	{
		return $this->belongsTo('App\Projeto');
	}

	public function responsaveis()
	{
		return $this->hasMany('App\Responsavel', 'imo_id');
	}

}
