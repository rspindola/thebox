@extends('layout')
@section('content')




<div class="sub-header">
	<div class="row">
		<div class="col-sm-7 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="{{ route('home') }}">Dashboard</a></li>
				<li><a href="{{ route('estatisticas.index', ['id' => $Dados->id ]) }}">{!! $Dados->projeto !!}</a></li>
				<li class="active">Calltacker</li>
			</ol>
		</div>
		<div class="col-sm-5 col-xs-12">

			<div class="datePicker">
				<div id="reportrange">
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b>
				</div>
			</div>

			@if(Auth::user()->isAdmin())
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Visualizar
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="{{ route('estatisticas.index', ['id' => $id ]) }}"><i class="fa fa-line-chart"></i> Estatísticas</a></li>
					<li><a href="{{ route('cadastros.index', ['id' => $id ]) }}"><i class="fa fa-users"></i> Cadastros</a></li>
					<li><a href="{{ route('calltracker.index',  ['id' => $id ]) }}"><i class="fa fa-phone"></i> Call Tracker</a></li>
					<li><a href="{{ route('leads.index',  ['id' => $id ]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar leads</a></li>
				</ul>
			</div>
			@endif

		</div>
	</div>
</div>



<div class="container-fluid dash-content">
	<div class="row">
		<div class="col-xs-12 margem-topo">
			<div class="panel panel-inside">
				<div class="panel-body">

					<div class="panel-heading">Calltracker

					{!! Form::open(['route'=>['calltracker.excel', $id]]) !!}
					{!! Form::hidden('pro_id', $id, ['class' => 'pro_id']) !!}
					{!! Form::hidden('pagina', 'calltracker', ['class' => 'Pagina']) !!}
					{!! Form::hidden('inicio', $Inicial, ['class' => 'DataInicial DataTableAtualizar']) !!}
					{!! Form::hidden('final', $Final, ['class' => 'DataFinal DataTableAtualizar']) !!}
						<button type="submit" class="btn btn-default btn-xs extport"><i class="fa fa-download"></i> Exportar dados</button>
					{!! Form::close() !!}

					{!! Form::open(['route'=>['calltracker.audio', $id]]) !!}
					{!! Form::hidden('pro_id', $id, ['class' => 'pro_id']) !!}
					{!! Form::hidden('pagina', 'calltracker', ['class' => 'Pagina']) !!}
					{!! Form::hidden('inicio', $Inicial, ['class' => 'DataInicial DataTableAtualizar']) !!}
					{!! Form::hidden('final', $Final, ['class' => 'DataFinal DataTableAtualizar']) !!}
						<button type="submit" class="btn btn-default btn-xs extport hidden-xs"><i class="fa fa-download"></i> Exportar mp3</button>
					{!! Form::close() !!}
					</div>

					<hr>
					<div class="row">
						<div class="col-xs-12" style="overflow: scroll;">
							@if($Calltracker)
							<table data-toggle="table" id="cadastros" class="DataTableAjax table footable table-striped responsive nowrap nb-header" cellspacing="0" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th class="all">Origem</th>
										<th class="desktop">Callrouting</th>
										<th class="desktop">Telefone</th>
										<th class="desktop">Data</th>
										<th class="desktop">Horário</th>
										<th class="desktop">Duração da chamada</th>
										<th class="desktop">MP3</th>
										@if(Auth::user()->isAdmin())
										<th class="desktop" data-orderable="false"></th>
										@endif
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							@else
							<p>Plano de Tracking Telefônico não contratado. Para contratar entre em contato com o atendimento da sua conta.</p>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
