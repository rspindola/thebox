@extends('layout')
@section('content')




{!! Form::open(['route'=>['projetos.store']]) !!}
{!! Form::hidden('pro_id', $Dados['id'], ['class' => 'pro_id']) !!}
{!! Form::hidden('pagina', 'estatisticas', ['class' => 'Pagina']) !!}
{!! Form::hidden('inicio', $Inicial, ['class' => 'DataInicial DataTableAtualizar']) !!}
{!! Form::hidden('final', $Final, ['class' => 'DataFinal DataTableAtualizar']) !!}

<div class="sub-header">
	<div class="row">
		<div class="col-sm-7 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="{{ route('home') }}">Dashboard</a></li>
				<li><a href="{{ route('estatisticas.index', ['id' => $Dados->id ]) }}">{!! $Dados->projeto !!}</a></li>
				<li class="active">Estatísticas</li>
			</ol>
		</div>
		<div class="col-sm-5 col-xs-12">

			<div class="datePicker">
				<div id="reportrange">
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b>
				</div>
			</div>

			@if(Auth::user()->isAdmin())
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Visualizar
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="{{ route('estatisticas.index', ['id' => $id ]) }}"><i class="fa fa-line-chart"></i> Estatísticas</a></li>
					<li><a href="{{ route('cadastros.index', ['id' => $id ]) }}"><i class="fa fa-users"></i> Cadastros</a></li>
					<li><a href="{{ route('calltracker.index',  ['id' => $id ]) }}"><i class="fa fa-phone"></i> Call Tracker</a></li>
					<li><a href="{{ route('leads.index',  ['id' => $id ]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar leads</a></li>
				</ul>
			</div>
			@endif
		</div>
	</div>
</div>



<div class="container-fluid dash-content">

	<div class="row margem-topo bloco1-estatisticas">
		<!-- Analise -->
		<div class="col-sm-6">
			<div class="panel panel-inside item-1">
				<div class="panel-body">
					<div class="panel-heading">Analise de origens no periodo</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoOrigens" style="width:100%;"></canvas>
								<div class="alerta-empty">Não existem dados para o período</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Analise -->
		<div class="col-sm-6">
			<div class="panel panel-inside item-2">
				<div class="panel-body">
					<div class="panel-heading">Metas</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoMetas" width="390" height="195"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Analytics -->
	<div class="row bloco2-analytics">
		<!-- Sessões -->
		<div class="col-lg-4 item-1">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading graph"><span>Sessões</span><h3 id="sessoesNumber">0</h3></div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoSessoes" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Usuários -->
		<div class="col-lg-4 item-2">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading graph"><span>Usuários</span><h3 id="usuariosNumber">0</h3></div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoUsuarios" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Visualizações de página -->
		<div class="col-lg-4 item-3">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading graph"><span>Visualizações de página</span><h3 id="visualizacaoNumber">0</h3></div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoVisualizacao" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Analytics Bloco 2-->
	<div class="row bloco3-analytics">
		<!-- Sessões -->
		<div class="col-lg-4 item-1">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading graph"><span>Páginas / sessão</span><h3 id="pgSessoesNumber">0</h3></div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoPaginasSessoes" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Usuários -->
		<div class="col-lg-4 item-2">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading graph"><span>Duração média da sessão</span><h3 id="tempoMedioNumber">0</h3></div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoTempoMedio" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Visualizações de página -->
		<div class="col-lg-4 item-3">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading graph"><span>Taxa de rejeição</span><h3 id="rejeicaoNumber">0</h3></div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoRejeicao" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Analytics Bloco 3-->
	<div class="row bloco3-analytics">
		<!-- Sessões -->
		<div class="col-lg-4 item-1">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading graph"><span>Porcentagem de novas sessões</span><h3 id="novasSessoesNumber">0</h3></div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoNovasSessoes" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Heatmap</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div id="healthmap" style="height: 300px; width:100%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Análise de Métricas
					<!-- <button type="button" class="btn btn-default btn-xs extport"><i class="fa fa-download"></i> Exportar dados</button> -->
					</div>
					<hr>
					<div class="row">
						<div class="col-xs-12" style="overflow: scroll;">
							<div>
								<table data-toggle="table" id="cadastros" class="DataTableAjax table table-striped responsive nowrap nb-header" cellspacing="0" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th class="all">Origem</th>
											<th class="min-tablet">Mídias</th>
											<th class="desktop">Total</th>
											<th class="desktop">Valor investido</th>
											<th class="desktop" >Valor final</th>
											<th class="desktop">Meta</th>
											<th class="desktop">Variação</th>
											<th class="desktop"  data-orderable="false"></th>
										</tr>
									</thead>
									<tbody>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>












	{!! Form::close() !!}



	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDg-0_KX0Q7DYzGtNpi_sdSZ0A_AnY52Z0&amp;libraries=visualization"></script>
	<script src="{{ asset('assets/Chart.min.js')}}"></script>
	<script src="{{ URL::to('/') }}/js/charts.js"></script>


	@endsection
