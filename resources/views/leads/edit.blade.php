@extends('layout')
@section('content')






<div class="sub-header">
    <div class="row">
        <div class="col-sm-7 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">Dashboard</a></li>
                <li><a href="{{ action('ProjetosController@index') }}">Projeto</a></li>
                <li><a href="{{ route('leads.index', ['id' => $pro_id ]) }}">Leads</a></li>
                <li class="active">Editar</li>
            </ol>
        </div>
    </div>
</div>



<div class="container-fluid dash-content">
    <div class="row">
        <div class="col-xs-12 margem-topo">
            <div class="panel panel-inside">
                <div class="panel-body">
                    <div class="panel-heading">{!! $Leads->origem !!} - Editar</div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12">


                            {!! Form::model($Dados,['method' => 'PATCH','route'=>['leads.update',$pro_id, $id]]) !!}

                            @include('leads.form')


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
