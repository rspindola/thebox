
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif



<div class="margem-topo">
    <div class="clone margem-topo-2x lead0valor">


        @foreach ($Dados as $Array)


        <div class="clone-inside">
           
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! form::text('inicio[]', $Array['inicio'], ['class' => 'form-control data-inicial-leads', 'id' => 'data-inicial-leads', 'placeholder' => 'Data Inicial' ]) !!}
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">

                        {!! form::text('fim[]', $Array['fim'], ['class' => 'form-control data-final-leads', 'id' => 'data-final-leads', 'placeholder' => 'Data Final' ]) !!}

                    </div>
                </div>


                <div class="col-sm-3">

                    <div class="input-group">
                        <span class="input-group-addon">R$</span>
                        {!! form::text('investido[]', $Array['investido'], ['class' => 'form-control valor-investido', 'id' => 'valor-investido', 'placeholder' => 'Valor Investido' ]) !!}
                    </div>
                </div>


                <div class="col-sm-3">
                    <div class="input-group">
                        <span class="input-group-addon">R$</span>
                        {!! form::text('meta[]', $Array['meta'], ['class' => 'form-control meta', 'id' => 'meta', 'placeholder' => 'Valor Meta' ]) !!}
                    </div>
                </div>


                <div class="col-sm-2 col-xs-12 btn-clone-inside">
                    <button type="button" class="AddValor btn btn-success"><span>Adicionar </span><i class="fa fa-plus"></i></button>
                    <button type="button" class="DeletaValor btn btn-danger"><span>Remover </span><i class="fa fa-minus"></i></button>
                    <div class="col-lg-3">
                    </div>
                </div>
            </div>
             <hr>
        </div>

        @endforeach


    </div>
</div>

<div class="margem-topo">
    <button type="submit" class="btn btn-primary">
        Salvar
    </button>
</div>
