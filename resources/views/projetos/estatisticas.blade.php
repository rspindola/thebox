@extends('layout')
@section('content')




{!! Form::open(['route'=>['projetos.store']]) !!}
{!! Form::hidden('pro_id', $Dados['id'], ['class' => 'pro_id']) !!}
{!! Form::hidden('inicio', null, ['class' => 'DataInicial']) !!}
{!! Form::hidden('final', null, ['class' => 'DataFinal']) !!}

<div class="sub-header">
	<div class="row">
		<div class="col-sm-7 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="{{ route('home') }}">Dashboard</a></li>
				<li><a href="{{ route('projetos.estatisticas', ['id' => $Dados->id ]) }}">{!! $Dados->projeto !!}1</a></li>
				<li class="active">Estatísticas</li>
			</ol>
		</div>
		<div class="col-sm-5 col-xs-12">
			<div class="datePicker">
				<div id="reportrange">
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container-fluid dash-content">
	
	<div class="row margem-topo bloco1-estatisticas">
		<!-- Analise -->
		<div class="col-sm-6">
			<div class="panel panel-inside item-1">
				<div class="panel-body">
					<div class="panel-heading">Analise de origens no periodo</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoOrigens" style="width:100%;"></canvas>
								<div id="legenda" class="chart-legend"></div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>

		<!-- Analise -->
		<div class="col-sm-6">
			<div class="panel panel-inside item-2">
				<div class="panel-body">
					<div class="panel-heading">Metas</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoMetas" width="390" height="195"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Analytics -->
	<div class="row bloco2-analytics">
		<!-- Sessões -->
		<div class="col-lg-4 item-1">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Sessões</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoSessoes" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Usuários -->
		<div class="col-lg-4 item-2">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Usuários</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoUsuarios" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Visualizações de página -->
		<div class="col-lg-4 item-3">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Visualizações de página</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoVisualizacao" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Analytics Bloco 2-->
	<div class="row bloco3-analytics">
		<!-- Sessões -->
		<div class="col-lg-4 item-1">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Páginas / sessão</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoPaginasSessoes" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Usuários -->
		<div class="col-lg-4 item-2">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Duração média da sessão</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoTempoMedio" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Visualizações de página -->
		<div class="col-lg-4 item-3">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Taxa de rejeição</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoRejeicao" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Analytics Bloco 3-->
	<div class="row bloco3-analytics">
		<!-- Sessões -->
		<div class="col-lg-4 item-1">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Porcentagem de novas sessões</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div style="width:100%;">
								<canvas id="GraficoNovasSessoes" width="800" height="260">
								</canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Healtmap</div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div id="healthmap" style="height: 300px; width:100%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-inside">
				<div class="panel-body">
					<div class="panel-heading">Leads <button type="button" class="btn btn-default btn-xs extport"><i class="fa fa-download"></i> Exportar dados</button></div>
					<hr>
					<div class="row">
						<div class="col-xs-12">
							<div>
								<table data-toggle="table" id="cadastros" class="table footable table-striped nb-header" cellspacing="0" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th class="all">Origem</th>
											<th class="desktop">Peça</th>
											<th class="mini-tablet">Total</th>
											<th class="desktop">Valor investido</th>
											<th class="desktop">Valor final</th>
											<th class="desktop" >Meta</th>
											<th class="desktop">Variação</th>
											<th class="desktop" data-orderable="false"></th>
										</tr>
									</thead>
									<tbody>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>












	{!! Form::close() !!}



    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDg-0_KX0Q7DYzGtNpi_sdSZ0A_AnY52Z0&amp;"></script>
    <script src="{{ URL::to('/') }}/js/charts.js"></script>


	@endsection
