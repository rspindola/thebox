@extends('layout')
@section('content')








<div class="sub-header">
	<div class="row">
		<div class="col-sm-7 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="{{ route('home') }}">Dashboard</a></li>
				<li><a href="{{ route('projetos.estatisticas', ['id' => $Dados->id ]) }}">{!! $Dados->projeto !!}1</a></li>
				<li class="active">Cadastros</li>
			</ol>
		</div>
		<div class="col-sm-5 col-xs-12">
			<div class="datePicker">
				<div id="reportrange">
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container-fluid dash-content">
	<div class="row">
		<div class="col-xs-12 margem-topo">
			<div class="panel panel-inside">
				<div class="panel-body">
					{!! Form::open(['route'=>['cadastros.excel']]) !!}
					{!! Form::hidden('pro_id', $id, ['class' => 'pro_id']) !!}
					{!! Form::hidden('pagina', 'cadastrosdados', ['class' => 'Pagina']) !!}
					{!! Form::hidden('inicio', $Inicial, ['class' => 'DataInicial DataTableAtualizar']) !!}
					{!! Form::hidden('final', $Final, ['class' => 'DataFinal DataTableAtualizar']) !!}
					<div class="panel-heading">Cadastros<button type="submit" class="btn btn-default btn-xs extport"><i class="fa fa-download"></i> Exportar dados</button></div>
					{!! Form::close() !!}
				<hr>
				<div class="row">
					<div class="col-xs-12">
						<table data-toggle="table" id="cadastros" class="DataTableAjax table table-striped nb-header" cellspacing="0" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th class="all">Peça</th>
									<th class="min-tablet">Nome</th>
									<th class="desktop">Telefone</th>
									<th class="desktop">Data do contato</th>
									<th class="desktop">Mensagem</th>
									<th class="desktop">Encaminhado para</th>
									<th class="desktop" data-orderable="false"></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


@endsection
