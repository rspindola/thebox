@extends('layout')
@section('content')











<div class="sub-header">
    <div class="row">
        <div class="col-sm-7 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ action('ProjetosController@index') }}">Dashboard</a></li>
                <li><a href="{{ action('ProjetosController@index') }}">Projeto</a></li>
                <li class="active">Editar</li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid dash-content">
    <div class="row">
        <div class="col-xs-12 margem-topo">
            <div class="panel panel-inside">
                <div class="panel-body">
                    <div class="panel-heading">{!!$Dados['projeto'] !!} - Editar</div>
                    <hr>

                    <div class="row">
                        <div class="col-xs-12">
                            {!! Form::model($Dados,['method' => 'PATCH','route'=>['projetos.update', $Dados['id']]]) !!}
                            @include('projetos.form')


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




























@endsection
