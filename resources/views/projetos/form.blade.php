<div class="row margem-topo">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="form-group">
        {!! Form::hidden('id', null, ['class' => 'pro_id']) !!}

            {!! form::label('projeto', 'Nome do projeto') !!}
            {!! form::text('projeto', null, ['class' => 'form-control', 'id' => 'projeto', 'placeholder' => 'Projeto' ]) !!}

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        {!! form::label('id', 'ID do projeto') !!}
        <div class="input-group">
            {!! form::text('id', null, ['class' => 'form-control', 'id' => 'id-coppy', 'placeholder' => 'ID', 'readonly' ]) !!}
            <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="btn-copy-id">Copiar</button>
            </span>
        </div>
    </div>
    <div class="col-md-2 col-sm-2 col-xs-12">
        <div class="form-group">

            {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
            {!! Form::select('status', ['1' => 'Ativo', '0' => 'Inativo'], null, ['class' => 'form-control']) !!}

        </div>
    </div>

     <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="form-group">
            {!! form::label('analytics', 'ID da vista (Analytics)') !!}
            {!! form::text('analytics', null, ['class' => 'form-control', 'id' => 'analytics', 'placeholder' => 'Ex: 98957503' ]) !!}

        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="form-group">
        <?php
            $meta = '';
        if(isset($Dados['meta'])) {
            foreach ($Dados['meta'] as $Array) {
                $meta .= $Array['meta'] . ', ';
            }
        }
        ?>

            {!! form::label('meta', 'Título - Metas') !!}
            {!! form::text('meta', $meta, ['class' => 'form-control', 'id' => 'Meta', 'placeholder' => 'Ex: 5' ]) !!}

        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="form-group">
        <?php
            $Calltracker = '';
        if(isset($Dados['telefone'])) {
            foreach ($Dados['telefone'] as $Array) {
                $Calltracker .= $Array['telefone'] . ', ';
            }
        }
        ?>
            {!! form::label('calltracker', 'Telefones associados ao calltracker') !!}
            {!! form::text('calltracker', $Calltracker, ['class' => 'form-control', 'id' => 'calltracker-tels', 'placeholder' => 'Ex: 21 0000-0000' ]) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-xs-12">
        <hr>
        <div class="form-group">
            {!! form::label('usuarios', 'Associar usuários ao projeto') !!}
            {!! form::text('usuarios', null, ['class' => 'form-control', 'id' => 'user-project', 'placeholder' => 'Ex: Usuário' ]) !!}
        </div>
    </div>

</div>














<div class="row margem-topo">
    <div class="col-lg-12">
        <h2>Imobiliárias responsáveis</h2>
        <hr>
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <button type="button" class="AddImobiliaria btn btn-primary btn-block" ><i class="fa fa-plus"></i> Adicionar imobiliária</button>
            </div>
        </div>
        @foreach ($Dados['imobiliaria'] as $key => $row)

        <!-- Responsáveis -->

        <div class="well clone margem-topo-2x Imobiliaria">
            <button type="button" class="DeletaImobilaria btn btn-danger delete-imob">Remover <i class="fa fa-times"></i>
            </button>
            <div class="row">

                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="imobiliaria" class="label-clone">Imobiliária</label>
                        {!! form::text('imobiliaria['.$key.']', $row['imobiliaria'], ['class' => 'form-control ImobiliariaNome', 'id' => $key, 'placeholder' => 'Nome da imobiliária' ]) !!}
                        {!! Form::hidden('imo_id['.$key.']', $row['id'], ['class' => 'ImobiliariaId']) !!}

                    </div>
                </div>
            </div>

            @if(isset($row['responsaveis']))
            @foreach ($row['responsaveis'] as $responsaveis)

            
            <div class="clone-inside">
                <hr>
                <div class="row">
                    <div class="col-sm-4 col-md-5">
                        <div class="form-group">
                            {!! form::text('responsavel['.$key.'][]', $responsaveis['responsavel'], ['class' => 'form-control Responsavel', 'id' => 'responsavel', 'placeholder' => 'Responsável' ]) !!}
                            {!! Form::hidden('res_id['.$key.'][]', $responsaveis['id'], ['class' => 'ResponsavelId']) !!}
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-5">
                        <div class="form-group">
                            {!! form::text('email['.$key.'][]', $responsaveis['email'], ['class' => 'form-control Email', 'id' => 'email', 'placeholder' => 'E-mai' ]) !!}
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-2 col-xs-12 btn-clone-inside">
                        <button type="button" id="{{ $key }}" class="AddResponsavel btn btn-success"><span>Adicionar </span><i class="fa fa-plus"></i></button>
                        <button type="button" class="DeletaResponsavel btn btn-danger"><span>Remover </span><i class="fa fa-minus"></i></button>
                    </div>
                </div>
            </div>

            @endforeach
            @endif
        </div>
        @endforeach


        <div class="margem-topo">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
