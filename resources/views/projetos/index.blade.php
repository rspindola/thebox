@extends('layout')
@section('content')

<div class="sub-header">
  <div class="row">
    <div class="col-sm-7 col-xs-12">
      <ol class="breadcrumb">
        <li><a href="#">Dashboard</a></li>
        <li class="active">Projetos</li>
      </ol>
    </div>
  </div>
</div>



<div class="container-fluid dash-content">
  <div class="row">
    <div class="col-xs-12 margem-topo">
      <div class="panel panel-inside">
        <div class="panel-body">
          <div class="panel-heading">Projetos</div>
          <hr>
          <div class="row">
            <div class="col-xs-12" style="overflow: scroll;">
              <table data-toggle="table" id="cadastros" class="DataTable table table-striped responsive nowrap nb-header" cellspacing="0" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th class="all">Titulo</th>
                    <th class="desktop">Status</th>
                    <th class="desktop">Criado em</th>
                    <th class="desktop" data-orderable="false" style="width: 200px; min-width: 200px"></th>
                  </tr>
                </thead>
                <tbody>

                 @foreach ($Projeto as $row)
                 <tr>
                   <td>{{ $row->projeto }}</td>
                   <td>
                      @if ($row->status == '1')
                          Ativo
                      @else
                          Inativo
                      @endif

                   </td>
                   <!--  <td>{{ $row->created_at }}</td> -->
                   <td>{{ $row->created_at }}</td>

                   <td>
                    @if(Auth::user()->isAdmin())
                    <div class="dropdown">
                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Visualizar
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="{{ route('estatisticas.index', ['id' => $row->id ]) }}"><i class="fa fa-line-chart"></i> Estatísticas</a></li>
                        <li><a href="{{ route('cadastros.index', ['id' => $row->id ]) }}"><i class="fa fa-users"></i> Cadastros</a></li>
                        <li><a href="{{ route('calltracker.index',  ['id' => $row->id ]) }}"><i class="fa fa-phone"></i> Call Tracker</a></li>
                        <li><a href="{{ route('leads.index',  ['id' => $row->id ]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar leads</a></li>
                      </ul>
                    </div>
                    @endif

                    <a href="{{route('projetos.edit',$row->id)}}" class="btn btn-edit btn-xs"><i class="fa fa-pencil"></i></a>

                    {!! Form::open(['method' => 'DELETE', 'route'=>['projetos.destroy', $row->id]]) !!}
                    <button type="submit" class="btn btn-delete btn-xs"><i class="fa fa fa-trash"></i></button>
                    {!! Form::close() !!}
                  </td>

                </tr>
                @endforeach



              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


@endsection
