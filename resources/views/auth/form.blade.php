@if (count($errors) > 0)
<div class="alert alert-danger">
    <!-- <strong>Whoops!</strong> There were some problems with your input.<br><br> -->
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif



<div class="row margem-topo">
    <div class="col-sm-5 col-xs-12">
        <div class="form-group">

            {!! Form::hidden('id', null, ['class' => 'usu_id']) !!}

            {!! form::label('name', 'Nome') !!}
            {!! form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nome' ]) !!}
        </div>
    </div>

    <div class="col-sm-5 col-xs-12">
        <div class="form-group">
            {!! form::label('email', 'Email') !!}
            {!! form::text('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'email@email.com' ]) !!}

        </div>
    </div>


    <div class="col-sm-2 col-xs-12">
        <div class="form-group">
            {!! Form::label('role', 'Tipo', ['class' => 'control-label']) !!}
            {!! Form::select('role', ['1' => 'Administrador', '0' => 'Usuário'], null, ['class' => 'form-control']) !!}

        </div>
    </div>


</div>

<div class="row">

    <div class="col-sm-2 col-xs-12">
        <div class="form-group">

            {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
            {!! Form::select('status', ['1' => 'Ativo', '0' => 'Inativo'], null, ['class' => 'form-control']) !!}


        </div>
    </div>



    <div class="col-sm-4 col-xs-12">
        <div class="form-group">
            {!! form::label('password', 'Senha') !!}
            {!! form::password('password',  ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Senha' ]) !!}

        </div>
    </div>
    <div class="col-sm-4 col-xs-12">
        <div class="form-group">
            {!! form::label('password_confirmation', 'Confirmar Senha') !!}
            {!! form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation', 'placeholder' => 'Confirmar senha' ]) !!}



        </div>
    </div>

    <div class="col-sm-4 col-xs-12">
        <div class="form-group">
            <label>Logotipo</label>
            <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
                        Carregar imagem... {!! Form::file('image') !!}
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>
            <img id='img-upload'/>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12 col-xs-12">
        <hr>
        <div class="form-group">
            {!! form::label('projeto', 'Associar projetos ao usuário') !!}
            {!! form::text('projeto', null, ['class' => 'form-control', 'id' => 'project-user', 'placeholder' => 'Ex: Projeto' ]) !!}

        </div>
    </div>

</div>

<div class="margem-topo">
    <button type="submit" class="btn btn-primary">
        Salvar
    </button>
</div>
