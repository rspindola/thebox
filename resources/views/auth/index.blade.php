@extends('layout')
@section('content')

<div class="sub-header">
  <div class="row">
    <div class="col-sm-7 col-xs-12">
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Dashboard</a></li>
        <li class="active">Usuários</li>
      </ol>
    </div>
  </div>
</div>


<div class="container-fluid dash-content">
  <div class="row">
    <div class="col-xs-12 margem-topo">
      <div class="panel panel-inside">
        <div class="panel-body">
          <div class="panel-heading">Usuários</div>
          <hr>
          <div class="row">
            <div class="col-xs-12" style="overflow: scroll;">
              <table data-toggle="table" id="cadastros" class="DataTable table table-striped responsive nowrap  nb-header" cellspacing="0" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th clas="all">Nome</th>
                    <th class="desktop">E-mail</th>
                    <th class="desktop">Função</th>
                    <th class="desktop">Criado em</th>
                    <th class="desktop"  data-type="html" data-sort-use="text" data-orderable="false" style="width: 100px; min-width: 100px"></th>
                  </tr>
                </thead>
                <tbody>

                 @foreach ($users as $row)
                 <tr>
                   <td>{{ $row->name }}</td>
                   <td>{{ $row->email }}</td>
                   <td>
                    @if($row->role == 1)
                    Administrador
                    @else
                    Usuário
                    @endif
                  </td>
                  <td>{{ $row->created_at }}</td>

                  <td>

                   <a href="{{route('usuarios.edit',$row->id)}}" class="btn btn-edit btn-xs"><i class="fa fa-pencil"></i></a>

                   {!! Form::open(['method' => 'DELETE', 'route'=>['usuarios.destroy', $row->id]]) !!}
                   <button type="submit" class="btn btn-delete btn-xs"><i class="fa fa fa-trash"></i></button>
                   {!! Form::close() !!}
                 </td>

               </tr>
               @endforeach



             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>


@endsection
