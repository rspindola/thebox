@extends('layout')
@section('content')






<div class="sub-header">
    <div class="row">
        <div class="col-sm-7 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">Dashboard</a></li>
                <li><a href="{{ action('Auth\AuthController@index') }}">Usuários</a></li>
                <li class="active">Adicionar</li>
            </ol>
        </div>
    </div>
</div>



<div class="container-fluid dash-content">
    <div class="row">
        <div class="col-xs-12 margem-topo">
            <div class="panel panel-inside">
                <div class="panel-body">
                    <div class="panel-heading">Adicionar</div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12">


                            {!! Form::open(['route'=>['usuarios.store'], 'files'=>true]) !!}
                            @include('auth.form')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
