@extends('layout')
@section('content')

<div class="sub-header">
  <div class="row">
    <div class="col-sm-7 col-xs-12">
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Dashboard</a></li>
        <li><a href="{{ action('ProjetosController@index') }}">Projeto</a></li>
        <li class="active">Leads</li>
      </ol>
    </div>

    @if(Auth::user()->isAdmin())
    <div class="col-sm-5 col-xs-12">
      <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          Visualizar
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li><a href="{{ route('estatisticas.index', ['id' => $id ]) }}"><i class="fa fa-line-chart"></i> Estatísticas</a></li>
          <li><a href="{{ route('cadastros.index', ['id' => $id ]) }}"><i class="fa fa-users"></i> Cadastros</a></li>
          <li><a href="{{ route('calltracker.index',  ['id' => $id ]) }}"><i class="fa fa-phone"></i> Call Tracker</a></li>
          <li><a href="{{ route('leads.index',  ['id' => $id ]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar leads</a></li>
        </ul>
      </div>
    </div>
    @endif

  </div>
</div>


<div class="container-fluid dash-content">
  <div class="row">
    <div class="col-xs-12 margem-topo">
      <div class="panel panel-inside">
        <div class="panel-body">
          <div class="panel-heading">Leads</div>
          <hr>
          <div class="row">
            <div class="col-xs-12" style="overflow: scroll;">
              <table data-toggle="table" id="leads" class="DataTable table responsive nowrap table-striped nb-header" cellspacing="0" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th class="all">Origem</th>
                    <th class="min-tablet" data-orderable="false" style="width: 100px; min-width: 100px"></th>
                  </tr>
                </thead>
                <tbody>

                 @foreach ($Dados as $row)
                 <tr>
                   <td>{{ $row->origem }}</td>

                   <td>


                     <a href="leads/{{$row->id}}" class="btn btn-edit btn-xs"><i class="fa fa-pencil"></i></a>

                     {!! Form::open(['method' => 'DELETE', 'route'=>['leads.destroy', $row->id]]) !!}
                     <button type="submit" class="btn btn-delete btn-xs"><i class="fa fa fa-trash"></i></button>
                     {!! Form::close() !!}
                   </td>

                 </tr>
                 @endforeach



               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>


@endsection
