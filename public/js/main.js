
$( document ).ready(function() {


	$('body').on('click', '.AddImobiliaria', function(){
		var qtd 		= $('.ImobiliariaNome').length;

		var novalinha   = $('.Imobiliaria:first').clone();
		novalinha.find("input").val("");
		novalinha.find(".Responsavel").attr('name', 'responsavel['+qtd+'][]');
		novalinha.find(".Email").attr('name', 'email['+qtd+'][]');
		novalinha.find(".ImobiliariaNome").attr('name', 'imobiliaria['+qtd+']');
		novalinha.find(".ImobiliariaId").attr('name', 'imo_id['+qtd+']');
		novalinha.find(".AddResponsavel").attr('id', qtd);
		novalinha.insertAfter('.Imobiliaria:last');
	});

	$('body').on('click', '.DeletaImobilaria', function(){
		//var tr = $(this).closest("div").remove();
		$('.DeletaImobilaria').closest('.Imobiliaria').not(':first').last().remove();
	});



	$('body').on('click', '.AddResponsavel', function(){
		var Localizacao 	= $(this).parent().parent().parent();
		var qual 			= $(this).attr('id');
		var novalinha   	= $('.clone-inside:first').clone();
		novalinha.find("input").val("");
		novalinha.find(".Responsavel").attr('name', 'responsavel['+qual+'][]');
		novalinha.find(".Email").attr('name', 'email['+qual+'][]');
		novalinha.find(".ImobiliariaNome").attr('name', 'imobiliaria['+qual+']');
		novalinha.find(".ImobiliariaId").attr('name', 'imo_id['+qual+']');

		novalinha.insertAfter(Localizacao);
	});

	$('body').on('click', '.DeletaResponsavel', function(){
		console.log("tes");
		// var tr = $(this).parent().parent().parent().remove();
		$(this).closest('.Imobiliaria').find('.clone-inside').not(':first').last().remove();
		// $('.DeletaResponsavel').closest('.Imobiliaria').find('.clone-inside').not(':first').last().remove();
	});


	/* AddValor */

	$('body').on('click', '.AddValor', function(){
		var Localizacao 	= $(this).parent().parent().parent();

		var novalinha   	= $('.clone-inside:first').clone();
		novalinha.find("input").val("");
		novalinha.insertAfter(Localizacao);

		$('.valor-investido').maskMoney({thousands:'.', decimal:',', symbolStay: true});
		$('.meta').maskMoney({thousands:'.', decimal:',', symbolStay: true});
		DatePickerLeads();
	});

	$('body').on('click', '.DeletaValor', function(){
		// var tr = $(this).parent().parent().parent().remove();
		$('.DeletaValor').closest('.clone').find('.clone-inside').not(':first').last().remove();

	});


	$('body').on('click', '.ModalMensagem', function(){
		var id = $(this).attr('id');
		var mensagem  = $('.ModalMensagem'+id).val();
		$('.ModalTitulo').html('Mensagem');
		$('.ModalDados').html(mensagem);
		$('[data-remodal-id=modal]').remodal().open();
	});



	$('body').on('click', '.ModalEncaminhado', function(){
		var id = $(this).attr('id');
		var encaminhamento  = $('.ModalEncaminhado'+id).val();
		$('.ModalTitulo').html('Encaminhado');
		$('.ModalDados').html(encaminhamento);
		$('[data-remodal-id=modal]').remodal().open();
	});



	$('body').on('click', '.ModalMidia', function(){
		var id = $(this).attr('id');
		var mensagem  = $('.ModalMidia'+id).val();
		$('.ModalTitulo').html('Mídias');
		$('.ModalDados').html(mensagem);
		$('[data-remodal-id=modal]').remodal().open();
	});



	/**
	 *
	 * Altura padrão para os ítens
	 *
	 */
	 jQuery.fn.extend({
	 	clearAttr: function(){
	 		var element = $(this);
	 		$.each(element,function(i,change){
	 			$(change).removeAttr("style");
	 		});
	 		return $(this);
	 	}
	 });

	 jQuery.fn.extend({
	 	adjustHeight: function(){
	 		var element = $(this);
	 		var finalHeight = 300;
	 		$.each(element,function(i,compare){
	 			if($(compare).height() > finalHeight){
	 				finalHeight = $(compare).height();
	 			}
	 		});
	 		$.each(element,function(i,change){
	 			$(change).height(finalHeight);
	 		});
	 		return $(this);
	 	}
	 });

	 ajustHeights = function(){
	 	var wWidth = $(window).width();
	 	$('.bloco1-estatisticas .item-1, .bloco1-estatisticas .item-2').clearAttr();
	 	if(wWidth>768){
	 		$('.bloco1-estatisticas .item-1, .bloco1-estatisticas .item-2').adjustHeight();
	 	}
	 }


	/**
	 *
	 * Menu toggle
	 *
	 */

	 $('#menu-toggle').click(function() {
	 	$('body').toggleClass( 'contract' );
	 	$('.sub').parent().removeClass( 'expand' );
	 });

	 /* Toggle dropdown *fade* */
	 $('.dropdown-toggle').click(function () {
	 	$(this).next('.dropdown-menu').fadeToggle(300);

	 });

	 $('.sub').click(function () {
	 	$(this).parent().toggleClass( 'expand' );
	 	if($('body').hasClass( "contract" )){
	 		$('body').removeClass( 'contract' );
	 	}
	 });

	 loginResize = function(){
	 	var wHeight = $(window).height();
	 	var pHeight = $('.panel-content').height();
	 	if(pHeight<wHeight){
	 		var centerPanel = (wHeight - pHeight)/2;
	 		$('.panel-content').css( "margin-top", centerPanel );
	 	}
	 }


	/**
	 *
	 * Data Table
	 *
	 */

	 $('.DataTable').DataTable( {
	 	 
	 	"bSort": false,
	 	autoWidth: false,
	 	"lengthMenu": [[50, 100, 200, 300], [50, 100, 200, 300]],
	 	language:
	 	{
	 		"sEmptyTable": "Nenhum registro encontrado",
	 		"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
	 		"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
	 		"sInfoFiltered": "(Filtrados de _MAX_ registros)",
	 		"sInfoPostFix": "",
	 		"sInfoThousands": ".",
			"sLengthMenu": "_MENU_", //resultados por página
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "", //Pesquisar
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		}

	} );

	 

	// Cria um 
	var DataTable	= $('.DataTableAjax').DataTable( {
		"processing": true,
		"serverSide": true,
		"bSort": false,
		"destroy":true,
		"lengthMenu": [[50, 100, 200, 300], [50, 100, 200, 300]],
		 autoWidth: false,

		"ajax": 'http://'+ window.location.hostname + '/'+$('.Pagina').val()+ '/' + $('.pro_id').val() + '/datatable/?inicio=' + $('.DataInicial').val() + '&final=' + $('.DataFinal').val(),

		'language': {
			'loadingRecords': "<img src='/Content/ajax-loader.gif' />"
		},

		language:
		{
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
				"sLengthMenu": "_MENU_", //resultados por página
				"sLoadingRecords": "Carregando...",
				"sProcessing": "Processando...",
				"sZeroRecords": "Nenhum registro encontrado",
      			"sSearch": "", //Pesquisar
      			"oPaginate": {
      				"sNext": "Próximo",
      				"sPrevious": "Anterior",
      				"sFirst": "Primeiro",
      				"sLast": "Último"
      			},
      			"oAria": {
      				"sSortAscending": ": Ordenar colunas de forma ascendente",
      				"sSortDescending": ": Ordenar colunas de forma descendente"
      			}
      		}

       });
	

	// Atualiza o datatable do calltracker
	$("#reportrange span").on('DOMSubtreeModified', function () {
		DataTable.ajax.url( 'http://'+ window.location.hostname + "/"+ $('.Pagina').val() +"/"+ $('.pro_id').val() +  '/datatable/?inicio=' + $('.DataInicial').val() + '&final=' + $('.DataFinal').val() ).load();
	});


	$('.dataTables_wrapper .row:first-child div.col-sm-6, .dataTables_wrapper .row:first-child div.col-sm-6').addClass('col-xs-6');


	/**
	 *
	 * DatePicker
	 *
	 */


	//http://www.daterangepicker.com/
	//http://momentjs.com/
	moment.locale('fr', {
		months : "janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro".split("_"),
		monthsShort : "jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez".split("_"),
		weekdays : "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
		weekdaysShort : "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
		weekdaysMin : "Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),
		longDateFormat : {
			LT : "HH:mm",
			LTS : "HH:mm:ss",
			L : "DD/MM/YYYY",
			LL : "D MMMM YYYY",
			LLL : "D MMMM YYYY LT",
			LLLL : "dddd D MMMM YYYY LT"
		},
		calendar : {
			sameDay: "[Aujourd'hui à] LT",
			nextDay: '[Demain à] LT',
			nextWeek: 'dddd [à] LT',
			lastDay: '[Hier à] LT',
			lastWeek: 'dddd [dernier à] LT',
			sameElse: 'L'
		},
		relativeTime : {
			future : "dans %s",
			past : "il y a %s",
			s : "quelques secondes",
			m : "une minute",
			mm : "%d minutes",
			h : "une heure",
			hh : "%d heures",
			d : "un jour",
			dd : "%d jours",
			M : "un mois",
			MM : "%d mois",
			y : "une année",
			yy : "%d années"
		},
		ordinalParse : /\d{1,2}(er|ème)/,
		ordinal : function (number) {
			return number + (number === 1 ? 'er' : 'ème');
		},
		meridiemParse: /PD|MD/,
		isPM: function (input) {
			return input.charAt(0) === 'M';
		},
		meridiem : function (hours, minutes, isLower) {
			return hours < 12 ? 'PD' : 'MD';
		},
		week : {
        dow : 1, // Segunda-feira é o primeiro dia da semana.
        doy : 4  // A semana que contém 4 de janeiro é a primeira semana do ano.
    }
});


	function cb(start, end) {
		$('.DataInicial').val(start.format('YYYY-MM-DD'));
		$('.DataFinal').val(end.format('YYYY-MM-DD'));
		$('#reportrange span').html(start.format('DD MMM, YYYY') + ' - ' + end.format('DD MMM, YYYY'));
	}
	cb(moment().subtract(29, 'days'), moment());

	$('#reportrange').daterangepicker({
		ranges: {
			
			'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
			'Últimos 15 dias': [moment().subtract(14, 'days'), moment()],
			'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
			'Últimos 60 dias': [moment().subtract(59, 'days'), moment()],
			'Esse mês': [moment().startOf('month'), moment().endOf('month')],
			'Mês passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		"locale": {
			"format": "DD/MM/YYYY",
			"separator": " - ",
			"applyLabel": "Aplicar",
			"cancelLabel": "Cancelar",
			"fromLabel": "A partir de",
			"toLabel": "Para",
			"customRangeLabel": "Customizar",
			"daysOfWeek": [
			"Dom",
			"Seg",
			"Ter",
			"Qua",
			"Qui",
			"Sex",
			"Sab"
			],
			"monthNames": [
			"Janeiro",
			"Fevereiro",
			"Março",
			"Abril",
			"Maio",
			"Junho",
			"Julho",
			"Agosto",
			"Setembro",
			"Outubro",
			"Novembro",
			"Dezembro"
			],
			"firstDay": 1
		},
		startDate: moment().subtract(29, 'days'),
		endDate: new Date(),
		"opens": "left"
	}, cb);


/**
 *
 * Resize Functions
 *
 */


 $(window).resize(function() {
 	loginResize();
 	ajustHeights();
 });

/**
 *
 * Inicializa funções
 *
 */

 loginResize();
 ajustHeights();


/**
 *
 * Upload
 *
 */

 $(document).on('change', '.btn-file :file', function() {
 	var input = $(this),
 	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
 	input.trigger('fileselect', [label]);
 });

 $('.btn-file :file').on('fileselect', function(event, label) {
 	
 	var input = $(this).parents('.input-group').find(':text'),
 	log = label;
 	
 	if( input.length ) {
 		input.val(log);
 	} else {
 		if( log ) alert(log);
 	}
 	
 });
 function readURL(input) {
 	if (input.files && input.files[0]) {
 		var reader = new FileReader();
 		
 		reader.onload = function (e) {
 			$('#img-upload').attr('src', e.target.result);
 		}
 		
 		reader.readAsDataURL(input.files[0]);
 	}
 }

 $("#imgInp").change(function(){
 	readURL(this);
 }); 	



 $('form').bind("keypress", function(e) {

 	if($(this).hasClass("form-login")){}else{
 		if (e.keyCode == 13) {               
 			e.preventDefault();
 			return false;
 		}

 	}
 });
 




 /* DatePikcer Leads */
 DatePickerLeads = function(){
 	$('.data-inicial-leads, .data-final-leads').daterangepicker({
 		singleDatePicker: true,
 		showDropdowns: true,
 		"locale": {
 			"format": "DD/MM/YYYY",
 			"separator": " - ",
 			"applyLabel": "Aplicar",
 			"cancelLabel": "Cancelar",
 			"fromLabel": "A partir de",
 			"toLabel": "Para",
 			"customRangeLabel": "Customizar",
 			"daysOfWeek": [
 			"Dom",
 			"Seg",
 			"Ter",
 			"Qua",
 			"Qui",
 			"Sex",
 			"Sab"
 			],
 			"monthNames": [
 			"Janeiro",
 			"Fevereiro",
 			"Março",
 			"Abril",
 			"Maio",
 			"Junho",
 			"Julho",
 			"Agosto",
 			"Setembro",
 			"Outubro",
 			"Novembro",
 			"Dezembro"
 			],
 			"firstDay": 1
 		},
 		startDate: new Date(),
 	})
 }

 DatePickerLeads();



 /* Máscara R$ */

 $('.valor-investido').maskMoney({thousands:'.', decimal:',', symbolStay: true});
 $('.meta').maskMoney({thousands:'.', decimal:',', symbolStay: true});



 /* Copiar ID do projeto */

 $("#btn-copy-id").on( "click", function() {
 	copyToClipboardMsg(document.getElementById("id-coppy"), "msg");
 });


 function copyToClipboardMsg(elem, msgElem) {
 	var succeed = copyToClipboard(elem);
 	var msg;
 	if (!succeed) {
 		msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
 	} else {
 		msg = "Text copied to the clipboard."
 	}
 	if (typeof msgElem === "string") {
 		msgElem = document.getElementById(msgElem);
 	}
 	msgElem = msg;

 }

 function copyToClipboard(elem) {
	  // Criar elemento de texto oculto, se ele ainda não existir
	  var targetId = "_hiddenCopyText_";
	  var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
	  var origSelectionStart, origSelectionEnd;
	  if (isInput) {
        // Usar apenas o elemento de fonte original para a seleção e copiar
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // Usar um elemento de forma temporária para a seleção e copiar
        target = document.getElementById(targetId);
        if (!target) {
        	var target = document.createElement("textarea");
        	target.style.position = "absolute";
        	target.style.left = "-9999px";
        	target.style.top = "0";
        	target.id = targetId;
        	document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // Selecionar o conteúdo
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // Copiar a seleção
    var succeed;
    try {
    	succeed = document.execCommand("copy");
    } catch(e) {
    	succeed = false;
    }
    // Restaurar o foco original
    if (currentFocus && typeof currentFocus.focus === "function") {
    	currentFocus.focus();
    }
    
    if (isInput) {
        // estaurar a selecção anterior
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // limpar o conteúdo temporário
        target.textContent = "";
    }
    return succeed;
}



/* Projetos */

var projetos = new Bloodhound({
	datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	prefetch: { 
		url: 'http://'+ window.location.hostname + '/projeto/tag',
		cache: false
	}
	// remote: {
	// 	url: 'http://'+ window.location.hostname + '/projeto/tag'
	// }
});
projetos.initialize();

var projectUser = $('#project-user');
projectUser.tagsinput({
	itemValue: 'value',
	itemText: 'text',
	typeaheadjs: {
		name: 'projetos',
		displayKey: 'text',
		source: projetos.ttAdapter()
	}
});

$.getJSON('http://'+ window.location.hostname + '/usuarios/'+$('.usu_id').val()+'/tagselecionadas', function(data) {
	$.each(data, function(index) {  
		projectUser.tagsinput('add', { "value": data[index].value , "text": data[index].text      });
	});
});


$('#calltracker-tels').tagsinput('items'); 

$('#Meta').tagsinput('items'); 

/* Usuários */



var usuarios = new Bloodhound({
	datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	prefetch: { 
		url: 'http://'+ window.location.hostname + '/usuarios/tag',
		cache: false
	}
	// remote: {
	// 	url: 'http://'+ window.location.hostname + '/usuarios/tag'
	// }
});
usuarios.initialize();

var userProject = $('#user-project');
userProject.tagsinput({
	itemValue: 'value',
	itemText: 'text',
	typeaheadjs: {
		name: 'usuarios',
		displayKey: 'text',
		source: usuarios.ttAdapter()
	}
});

$.getJSON('http://'+ window.location.hostname + '/projeto/'+$('.pro_id').val()+'/tagselecionadas', function(data) {
	$.each(data, function(index) {  
		userProject.tagsinput('add', { "value": data[index].value , "text": data[index].text      });
	});
});

});