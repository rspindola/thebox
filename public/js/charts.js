
$( document ).ready(function() {

	var Graficos = [];
	
	// var randomScalingFactor = function() {
	// 	return Math.round(Math.random() * 100);
 //            //return 0;
 //        };
 var randomColorFactor = function() {
 	return Math.round(Math.random() * 255);
 };

 $("#reportrange span").on('DOMSubtreeModified', function () {
 	var projeto = $('.pro_id').val();
 	var Inicio 	= $('.DataInicial').val();
 	var Final 	= $('.DataFinal').val();


 	$.ajax({
 		type: 'GET',
 		url: '/estatisticas/' + projeto,
 		data: {
 			inicio: Inicio,
 			final: Final
 		},
 		success: function (data) {

 			for (var i = 0; i < Graficos.length; i++) {
 				if (typeof Graficos[i] !== "undefined") {
 					Graficos[i].destroy();
 					Graficos[i].clear();

 					Graficos[i].update();
 				}
 			}

 			GraficoLinha('GraficoSessoes', data['GraficoSessoes']['Label'],  data['GraficoSessoes']['Data']);
 			GraficoLinha('GraficoUsuarios', data['GraficoUsuarios']['Label'],  data['GraficoUsuarios']['Data']);
 			GraficoLinha('GraficoVisualizacao', data['GraficoVisualizacao']['Label'],  data['GraficoVisualizacao']['Data']);
 			GraficoLinha('GraficoPaginasSessoes', data['GraficoPaginasSessoes']['Label'],  data['GraficoPaginasSessoes']['Data']);
 			GraficoLinha('GraficoTempoMedio', data['GraficoTempoMedio']['Label'],  data['GraficoTempoMedio']['Data']);
 			GraficoLinha('GraficoRejeicao', data['GraficoRejeicao']['Label'],  data['GraficoRejeicao']['Data']);
 			GraficoLinha('GraficoNovasSessoes', data['GraficoNovasSessoes']['Label'],  data['GraficoNovasSessoes']['Data']);

 			setTimeout(function(){ 


 				function formataNumero (num) {
 					return num
			       .toFixed(0) // remover dois dídigos decimais
			       .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") // usar . como separador
			   }

			   $('#sessoesNumber').text( formataNumero(data['GraficoSessoes']['Total']));
			   $('#usuariosNumber').text(formataNumero( data['GraficoUsuarios']['Total']));
			   $('#visualizacaoNumber').text(formataNumero( data['GraficoVisualizacao']['Total']));
			   $('#pgSessoesNumber').text( data['GraficoPaginasSessoes']['Total']);

			   function secondsTimeSpanToHMS(s) {
			    var h = Math.floor(s/3600); //Obtendo horas inteiras
			    s -= h*3600;
			    var m = Math.floor(s/60); //Obtendo minutos restantes
			    s -= m*60;
			    return (h < 10 ? '0'+h : h)+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero, formatação em minutos e segundos
			}


			$('#tempoMedioNumber').text(secondsTimeSpanToHMS(data['GraficoTempoMedio']['Total']));
			$('#rejeicaoNumber').text( data['GraficoRejeicao']['Total'] + '%');
			$('#novasSessoesNumber').text( data['GraficoNovasSessoes']['Total'] + '%');

		}, 500);

 			

 			var GraficosMetas = [];
 			var Metas = data['LabelMetas'].length;

 			for (var i = 1; i <= Metas; i++) {
 				GraficosMetas.push(data['GraficoMeta'+i]['Data']);
 			}

 			GraficoLinhaMetas(
 				data['GraficoMeta1']['Label'], 
 				$.each( GraficosMetas, function (dados) {
 					return dados;
 				}),
 				data['LabelMetas']
 				);


 			/* Gráfico de pizza */

 			GraficoPizza('GraficoOrigens', data['GraficoPizza']);



			// HeatMap
			HeatMapDados(data['HeatMap']);

		},
		error: function(result) {
		}

	});
 });	


	 /**
	  *
	  * Gráfico origens
	  *
	  */

	  function GraficoPizza(Grafico, Data) {


	  	/* convertendo o array */
	  	var jsonString = JSON.stringify(Data);
	  	var obj = JSON.parse(jsonString);

	  	/* Exibindo "alerta" */

	  	if(obj.labels != undefined){
	  		$('.alerta-empty').hide();
	  		$("#GraficoOrigens").show();
	  	}
	  	else{
	  		$('.alerta-empty').show();
	  		$("#GraficoOrigens").hide();
	  	}

	  	/* PORCENTAGEM */
	  	contador = 0;
	  	Qtditens = obj.data.length;
	  	// console.log(Qtditens);

	  	/* pegando os valores */
	  	$.each(obj.data, function (index, valores) {
	  		contador += +valores;
	  	});

	  	/* calculando a porcentagem */
	  	function percent(valor, contador){
	  		var percent = (valor * 100) / contador;
	  		return(" - " + percent.toString().substring(0,4) + "% ");
	  		console.log(percent.toString().substring(0,4));
	  	}


	  	/* Trazendo novos labels */
	  	newLabels = [];
	  	for (var i = 0; i < Qtditens; i++) {
	  		newLabel = obj.labels[i] + percent(obj.data[i], contador);
	  		newLabels.push(newLabel);
	  	}


	  	/* Variáveis do gráfico*/

	  	var origens = {
	  		datasets: [{
	  			data: obj.data,
	  			backgroundColor: obj.backgroundColor,
	  		}],
	  		// labels: obj.labels
	  		labels: newLabels
	  	};
	  	options = {
	  		responsive: true,

	  		legend: {
	  			position: 'bottom',
	  			labels: {
	  				boxWidth: 10,
	  				fontSize: 10,
	  				padding: 7,
	  			}
	  		},
	  		elements: {
	  			arc: {
	  				borderWidth: 0.2
	  			}
	  		}
	  	};



	  	var ctx = $("#GraficoOrigens").get(0).getContext("2d");
	  	Graficos.push(new Chart(ctx, {
	  		type: 'pie',
	  		data: origens,
	  		options: options,
	  		animation:{
	  			animateScale:true
	  		},

	  	}));

	  }


		/**
		 *
		 * Metas
		 *
		 */

		 function GraficoLinhaMetas(Label, DadosGrafico, LabelMetas) {

		 	var Dados = {labels: Label};
		 	Dados.datasets = [];

		 	var jsonString = JSON.stringify(LabelMetas);
		 	var varMetas = JSON.parse(jsonString);
		 	var qtdM = varMetas.length ;
		 	var counter = -1;

		 	var metaNames = function(){
		 		if(counter < qtdM){
		 			counter++;
		 			return varMetas[counter].trim();
		 		}else{return varMetas[counter];}
		 	}


		 	$.each(DadosGrafico, function (index, DadosAux, MetasAux) {

		 		rgb = randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() ;


		 		Dados.datasets.push({
		 			label: metaNames(),
		 			tension: 0.1,
		 			fillColor: 'rgba(' + rgb + ','+ 0.6 +')',
		 			strokeColor: 'rgba(' + rgb + ','+ 1 +')',
		 			pointColor: 'rgba(' + rgb + ','+ 1 +')',
		 			borderColor: 'rgba(' + rgb + ','+ 1 +')',
		 			backgroundColor: 'rgba(' + rgb + ','+ 0.6 +')',
		 			pointBackgroundColor: 'rgba(' + rgb + ','+ 1+')',
		 			borderWidth: 2,
		 			pointRadius: 2,
		 			data: DadosAux
		 		});


		 	});
		 	options = {
		 		type: 'line',
		 		responsive: true,
		 		pointStrokeColor: "#000",
		 		legend: {
		 			position: 'bottom',
		 		},
		 		tooltips: {
		 			mode: 'label'
		 		},
		 		hover: {
		 			mode: 'dataset'
		 		},
		 		legend: {
		 			position: 'bottom',
		 			labels: {
		 				boxWidth: 20,
		 				padding: 7,
		 			}
		 		},
		 		scales: {
		 			xAxes: [{
		 				display: false,
		 				scaleLabel: {
		 					show: true,
		 					labelString: 'Month'
		 				}
		 			}],
		 			yAxes: [{
		 				display: true,
		 				scaleLabel: {
		 					show: true,
		 					labelString: 'Value'
		 				},
		 				ticks: {
		 					suggestedMin: 0,
		 				}
		 			}]
		 		}
		 	}
		 	var ctx = $("#GraficoMetas").get(0).getContext("2d");
		 	Graficos.push(new Chart(ctx, {
		 		type: 'line',
		 		data: Dados,
		 		options: options
		 	}));


		 }


		 /**
		  *
		  * Analytics
		  *
		  */


		  function GraficoLinha(Grafico, Label, Data) {
		  	switch(Grafico) {
		  		case 'GraficoSessoes':
		  		var labelGrafico = "Sessões";
		  		break;
		  		case 'GraficoUsuarios':
		  		var labelGrafico = "Usuários";
		  		break;
		  		case 'GraficoVisualizacao':
		  		var labelGrafico = "Visualizações de página";
		  		break;
		  		case 'GraficoPaginasSessoes':
		  		var labelGrafico = "Páginas / sessão";
		  		break;
		  		case 'GraficoTempoMedio':
		  		var labelGrafico = "Duração média da sessão";
		  		break;
		  		case 'GraficoRejeicao':
		  		var labelGrafico = "Taxa de rejeição";
		  		break;
		  		case 'GraficoNovasSessoes':
		  		var labelGrafico = "% de novas sessões";
		  		break;
		  	}

		  	var Dados = {
		  		
		  		labels: Label,
		  		datasets: [
		  		{
		  			label: labelGrafico,
		  			tension: 0.1,
		  			fillColor: 'rgba(151,187,205,0.2)',
		  			strokeColor: 'rgba(151,187,205,1)',
		  			pointColor: 'rgba(151,187,205,1)',
		  			borderColor: 'rgba(151,187,205,1)',
		  			backgroundColor: 'rgba(151,187,205,0.2)',
		  			pointBackgroundColor :'rgba(151,187,205,1)',
		  			borderWidth: 2,
		  			pointRadius: 2,
		  			data: Data
		  		}
		  		]
		  	};

		  	options = {
		  		type: 'line',
		  		responsive: true,
		  		legend: {
		  			display: false,
		  		},
		  		tooltips: {
		  			mode: 'label'
		  		},
		  		hover: {
		  			mode: 'dataset'
		  		},
		  		scales: {
		  			xAxes: [{
		  				display: false,
		  				scaleLabel: {
		  					show: true,
		  					labelString: 'Month'
		  				}
		  			}],
		  			yAxes: [{
		  				display: true,
		  				scaleLabel: {
		  					show: true,
		  					labelString: 'Value'
		  				},
		  				ticks: {
		  					suggestedMin: 0,
		  				}
		  			}]
		  		}
		  	}

		  	var ctx = $("#"+Grafico).get(0).getContext("2d");
		  	Graficos.push(new Chart(ctx, {
		  		type: 'line',
		  		data: Dados,
		  		options: options
		  	}));

		  }


	  /**
	   *
	   * HEALTMAP
	   *
	   */


	   var map;
	   var Circles = [];
	   function initialize() {
	   	 
	   	    map = new google.maps.Map(document.getElementById("healthmap"), {
	   		        zoom: 10,
	   		        center: { lat: -22.8901657, lng: -43.3428286},
	   		        mapTypeId: google.maps.MapTypeId.ROADMAP,
	   		styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
	   	    });

	   }
	   initialize();



	   function HeatMapDados(Dados) {

	   	for (var i = 0; i < Circles.length; i++) {
	   		Circles[i].setMap(null);
	   	}
	   	var latlng = 	[];
	   	$.each(Dados, function(index, ponto) {

	   		latlng.push(new google.maps.LatLng(ponto.lat, ponto.lng));

	   	});

	   	Circles.push(new google.maps.visualization.HeatmapLayer({
	   		map:     map,
	   		data: latlng,
	   		radius: 12
	   	}));


	   }

	});


