
$( document ).ready(function() {

	var Graficos = [];

	$("#reportrange span").on('DOMSubtreeModified', function () {
		var projeto = $('.pro_id').val();
		var Inicio 	= $('.DataInicial').val();
		var Final 	= $('.DataFinal').val();
		$.ajax({
			type: 'GET',
			url: '/estatisticas/' + projeto,
			data: {
				inicio: Inicio,
				final: Final
			},
			success: function (data) {


				for (var i = 0; i < Graficos.length; i++) {
					if (typeof Graficos[i] !== "undefined") {
						Graficos[i].destroy();
						Graficos[i].clear();
					}
				}

				GraficoPiazza('GraficoOrigens', data['GraficoPizza']);

				GraficoLinha('GraficoSessoes', data['GraficoSessoes']['Label'],  data['GraficoSessoes']['Data']);
				GraficoLinha('GraficoUsuarios', data['GraficoUsuarios']['Label'],  data['GraficoUsuarios']['Data']);
				GraficoLinha('GraficoVisualizacao', data['GraficoVisualizacao']['Label'],  data['GraficoVisualizacao']['Data']);
				GraficoLinha('GraficoPaginasSessoes', data['GraficoPaginasSessoes']['Label'],  data['GraficoPaginasSessoes']['Data']);
				GraficoLinha('GraficoTempoMedio', data['GraficoTempoMedio']['Label'],  data['GraficoTempoMedio']['Data']);
				GraficoLinha('GraficoRejeicao', data['GraficoRejeicao']['Label'],  data['GraficoRejeicao']['Data']);
				GraficoLinha('GraficoNovasSessoes', data['GraficoNovasSessoes']['Label'],  data['GraficoNovasSessoes']['Data']);


				// Grafico de Meta
				GraficoLinhaMetas(data['GraficoMeta1']['Label'], [data['GraficoMeta1']['Data'], data['GraficoMeta2']['Data'], data['GraficoMeta3']['Data'], data['GraficoMeta4']['Data'], data['GraficoMeta5']['Data']]);

				// HeatMap
				HeatMapDados(data['HeatMap']);

			},
			error: function(result) {
			}

		});
	});	
	function GraficoPiazza(Grafico, Data) {
		options = {
			responsive: true
		};
		
		// Grafico de Pizza
		var ctx = $("#GraficoOrigens").get(0).getContext("2d");
		Graficos.push(new Chart(ctx).Pie(Data,options));

	}

	function GraficoLinha(Grafico, Label, Data) {


		var ctx = $("#"+Grafico).get(0).getContext("2d");

		var Dados = {
			labels: Label,
			datasets: [
			{
				label: "My Second dataset",
				fillColor: "rgba(151,187,205,0.2)",
				strokeColor: "rgba(151,187,205,1)",
				pointColor: "rgba(151,187,205,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(151,187,205,1)",
				data: Data
			}
			]
		};

		Graficos.push(new Chart(ctx).Line(Dados, 
		{ 
			responsive: true, 
			bezierCurve : false,
			scaleShowGridLines: false,
			scaleShowHorizontalLines: false,
			showScale: false,
			pointDot: false


		}));

	}


	function GraficoLinhaMetas(Label, DadosGrafico) {



		var Dados = {labels: Label};

		Dados.datasets = [];
		$.each(DadosGrafico, function (index, DadosAux) {

			Dados.datasets.push({
				label: "My Second dataset",
				fillColor: "rgba(151,187,205,0.2)",
				strokeColor: "rgba(151,187,205,1)",
				pointColor: "rgba(151,187,205,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(151,187,205,1)",
				data: DadosAux
			});


		});

		var ctx = $("#GraficoMetas").get(0).getContext("2d");

		Graficos.push(new Chart(ctx).Line(Dados, 
		{ 
			responsive: true, 
			bezierCurve : false,
			scaleShowGridLines: false,
			scaleShowHorizontalLines: false,
			showScale: false,
			pointDot: false


		}));

	}

	var map;
	var Circles = [];
	function initialize() {
		 
		    map = new google.maps.Map(document.getElementById("healthmap"), {
			        zoom: 10,
			        center: { lat: -22.8901657, lng: -43.3428286},
			        mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
		    });

	}
	initialize();



	function HeatMapDados(Dados) {

		for (var i = 0; i < Circles.length; i++) {
			Circles[i].setMap(null);
		}
		var latlng = 	[];
		$.each(Dados, function(index, ponto) {

			latlng.push(new google.maps.LatLng(ponto.lat, ponto.lng));

		});

		Circles.push(new google.maps.visualization.HeatmapLayer({
			map:     map,
			data: latlng,
			radius: 12
		}));


	}

});



