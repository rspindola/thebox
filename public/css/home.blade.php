@extends('layout')
@section('content')



            <div class="sub-header">
                <div class="row">
                    <div class="col-sm-7 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                        </ol>
                    </div>
                    <div class="col-sm-5 col-xs-12">
                        <!-- <div class="datePicker">
                            <div id="reportrange">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>


            <?php $cores = array('azul', 'roxo', 'rose', 'amarelo', 'azul', 'roxo', 'rose', 'amarelo'); $i =0; ?>


            <div class="container-fluid dash-content">
                <div class="row">
                    <div class="col-lg-12 margem-topo">
                        <div class="panel panel-inside">
                            <div class="panel-body">
                                <div class="panel-heading">Projetos</div>
                                <hr>
                                <div class="row">

                                @foreach ($Dados as $row)
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                        <a href="{{ route('estatisticas.index', ['id' => $row->id ]) }}" class="card margem-bottom-2x {!! $cores[$i] !!}">
                                            {!! $row->projeto !!}
                                            <?php if($i == 7) $i = 0; else $i++ ?>
                                        </a>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection
