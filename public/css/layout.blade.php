<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Dashboard Sensorial</title>




    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome-4.3.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/bootstraoTagsInput/bootstrap-tagsinput.css') }}">

    <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.7/remodal.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.7/remodal-default-theme.min.css" rel="stylesheet"/>

    {!! Html::style('css/app.css') !!}







</head>
<body class="contract">
    <div class="wrapper remodal-bg">
        <nav id="sidebar" role="navigation" -step="2" data-intro="Template has <b>many navigation styles</b>" data-position="right" class="navbar-default navbar-static-side" style="min-height: 100%;">
            <div class="sidebar-collapse menu-scroll" style="overflow: hidden;">
                <ul class="nav side-menu">
                    <!-- Logotipo -->
                    <li class="active logotipo">
                        <a href="{{ route('home')}}">
                            <h1 class="brand">
                                Sensorial
                            </h1>
                        </a>
                    </li>
                    <!-- Itens menu -->
                    <li>
                        <a href="{{ route('home')}}">
                            <i class="fa fa-tachometer"></i>
                            <span class="menu-title">Dashboard</span>
                        </a>
                    </li>

                    @if(Auth::user()->isAdmin())
                    <!-- Usuários -->
                    <li>
                        <a href="{{ action('Auth\AuthController@index')}}">
                            <i class="fa fa-users"></i>
                            <span class="menu-title">Usuários</span>
                        </a>
                    </li>

                    <!-- Usuários -->
                    <li>
                        <a href="{{ action('ProjetosController@index')}}">
                            <i class="fa fa-list-alt"></i>
                            <span class="menu-title">Administrar projetos</span>
                        </a>
                    </li>
                    @endif
                    @if($Menu)
                    @foreach ($Menu as $row)


                    <li>
                        <a href="#" class="sub">
                            <i class="fa fa-th-list"></i>
                            <span class="menu-title">{{ $row->projeto }}</span>
                        </a>
                        <ul>
                            <li><a href="{{ route('estatisticas.index', ['id' => $row->id ]) }}">
                                <i class="fa fa-line-chart"></i>
                                <span class="menu-title">Estatísticas</span>
                            </a></li>
                            <li><a href="{{ route('cadastros.index', ['id' => $row->id ]) }}">
                                <i class="fa fa-users"></i>
                                <span class="menu-title">Cadastros</span>
                            </a></li>

                            <li><a href="{{ route('calltracker.index',  ['id' => $row->id ]) }}">
                                <i class="fa fa-phone"></i>
                                <span class="menu-title">Call Tracker</span>
                            </a></li>

                            <li><a href="{{ route('leads.index',  ['id' => $row->id ]) }}">
                               <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                <span class="menu-title">Leads</span>
                            </a></li>

                        </ul>
                    </li>

                    @endforeach
                    @endif

                    <li>
                        <a href="{{ route('logout') }}">
                            <i class="fa fa-power-off"></i>
                            <span class="menu-title">Sair</span>
                        </a>
                    </li>

                </ul>
            </div>
        </nav>





        <div id="page-wrapper" class="content">
            <nav class="navbar navbar-top container-fluid">
                <div id="navbar" class="navbar-collapse collapse">

                  
                    <ul class="nav navbar-nav navbar-left">
                        <li><a id="menu-toggle" href="#"><i class="fa fa-bars"></i></a></li>
                          @if(Auth::user()->isAdmin())
                        <li class="hidden-xs"><a href="{{ route('projetos.create') }}"><i class="fa fa-plus"></i> Adicionar projeto</a></li>
                        <li class="hidden-xs"><a href="{{ route('usuarios.create') }}"><i class="fa fa-user-plus"></i> Adicionar usuário</a></li>
                          @endif
                    </ul>
                  

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <div class="user">
                                    <img src="{{ url('/') }}/img/Avatar/{{ Auth::user()->email }}" height="29" width="29">
                                    {{ Auth::user()->name }}<span class="caret"></span>
                                </div>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <!-- <li><a href="#"><i class="fa fa-user"></i>Minha Conta</a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i>Mensagens</a></li>
                                <li class="divider"></li> -->
                                <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i>Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>










            <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>


            @yield('content')













        </div>
    </div>


    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/bootstraoTagsInput/bootstrap-tagsinput.min.js')}}"></script>

    <script src="http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.7/remodal.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>




    <script src="{{ URL::to('/') }}/js/main.js"></script>


    <!--====  End of Remodal  ====-->

    <div class="remodal" data-remodal-id="modal">
        <button data-remodal-action="close" class="remodal-close"></button>
        <h1 class="ModalTitulo">Titulo</h1>
        <p class="ModalDados">
            Dados
        </p>
        <br>
        <!-- <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button> -->
        <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
    </div>


    @yield('scripts')
</body>
</html>
